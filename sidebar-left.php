<?php 
$theme = My_Theme::get_instance();
$layout = $theme->get_option('site_layout');
$layout = intval($layout);

if($layout == 2 || $layout == 4):
?>
<div class="secondary fl">
<div id="left-sidebar" class="left-sidebar">
<?php dynamic_sidebar( 'left-sidebar' )  ?>
</div>
</div>
<?php endif; ?>