<?php 
$previous = get_previous_post();
$next = get_next_post();
$previous_title ='';
$next_title ='';
if (!empty($next)) {
    $next_title = '<span class="meta-nav">&larr;</span> '.$next->post_title;
}
if (!empty($previous)) {
    $previous_title = $previous->post_title.' <span class="meta-nav">&rarr;</span>';
}
?>
<nav class="pager-single">
<span class="pager-next"><?php next_post_link( '%link', $next_title ); ?></span>
<span class="pager-previous"><?php previous_post_link( '%link',  $previous_title ); ?></span>
</nav>