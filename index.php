<?php get_header(); ?>
<div class="content-canvas">
<?php get_template_part( 'feature' ); ?>
<?php get_sidebar('left'); ?>
<main id="main-content" class="primary fl">
<?php 
get_template_part('ad','top');
$theme = My_Theme::get_instance();
$theme->get_loop_template();
?>
</main>
<?php get_sidebar('right'); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>