<div class="visually-hidden">
<?php
if ( has_post_thumbnail() ) : 
?>
<div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
<meta itemprop="url" content="<?php the_post_thumbnail_url(); ?>"/>
<meta itemprop="width" content="<?php echo My_Theme::THUMB_WIDTH; ?>">
<meta itemprop="height" content="<?php echo My_Theme::THUMB_HEIGHT; ?>">
</div>
<?php endif; ?>
<time itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>" class="date"></time>
<time itemprop="dateModified" datetime="<?php the_modified_date('Y-m-d'); ?>" class="date"></time>
<div itemprop="author" itemscope itemtype="https://schema.org/Person" class="h-card vcard author-about" >
<meta itemprop="name" content="<?php echo get_the_author_meta( 'display_name' );?>">
</div>
<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
<meta itemprop="name" content="<?php echo bloginfo('name');?>">
<?php
$logo_image = get_header_image();
if($logo_image) :
?>
<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
<meta itemprop="url" content="<?php echo $logo_image; ?>">
</div>
<?php endif; ?>
</div>
<link itemprop="mainEntityOfPage" href="<?php echo site_url(); ?>">
</div>