<?php get_header(); ?>
<div class="content-canvas">
<?php get_template_part( 'feature' ); ?>
<?php get_sidebar('left'); ?>
<main id="main-content" class="primary fl">
<?php 
get_template_part('ad','top');
$theme = My_Theme::get_instance();
$theme->breadcrumb();  
$html_tag = $theme->get_tag_for('tag_category_title');
?>
<div class="h-feed category-feed">
<?php
echo "<$html_tag class='p-name category-title'>";
if ( is_day() ){
	$cat_title = get_the_date();
} elseif ( is_month() ) {
	$cat_title = get_the_date( 'F Y' );
} elseif ( is_year() ) {
	$cat_title = get_the_date( 'Y' );
} else {
	$cat_title =  __( 'Blog Archives', THEME_NAME );
}
echo $cat_title;
echo "</$html_tag>";
?>
<?php
$theme->get_loop_template();
?>
</div>
</main>
<?php get_sidebar('right'); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>