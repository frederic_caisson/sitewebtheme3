<?php
get_header(); 
$theme = My_Theme::get_instance();
$hide_comments = $theme->get_option('hide_comments');
if(empty($hide_comments)){
$hide_comments = false;
} else {
$hide_comments = true;
}
?>
<div class="content-canvas">
<main id="main-content" role="main" class="fl full-page">
<?php the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
<?php get_template_part('header','entry'); ?>
<div class="entry-content">
<div class="entry-attachment">
<div class="attachment">
<?php
$next_href = '';
$prev_href = '';
if(isset($_GET['gallery'])){
$gallery = $_GET['gallery'];
$decoded = base64_decode( urldecode( $gallery ) );
$attachement_ids = explode(',',$decoded);
$count = count ($attachement_ids);

foreach ($attachement_ids as $i => $attachement_id) {
	if($post->ID == $attachement_id){
		if (($i - 1) < 0) {
		    $prev = null;
		} else {
		    $prev = $attachement_ids[$i - 1];
		}

		if (($i + 1) > ($count - 1)){
			$next = null;
		} else {
			$next = $attachement_ids[$i + 1];
		}
	}
}

if($next){
	$link = wp_get_attachment_link($next, 'full', true, false);
	$html = new SimpleXMLElement($link);
	$next_href = $html['href']."?gallery=$gallery";
}
if($prev){
	$link = wp_get_attachment_link($prev, 'full', true, false);
	$html = new SimpleXMLElement($link);
	$prev_href = $html['href']."?gallery=$gallery";;
}
$html = null;

}
?>
<?php
$original = wp_get_attachment_image_src( $post->ID, 'full');
$attachment_size = apply_filters( 'theme_attachment_size',  800 );
$imgmeta = wp_get_attachment_metadata( $post->ID );
?>
<a href="<?php echo $original[0]; ?>"><?php echo wp_get_attachment_image( $post->ID, array( $attachment_size, 9999 ) ); ?></a>
<div itemscope itemtype="http://schema.org/ImageObject">
<h2 itemprop="name"><?php echo $theme::get_attachement_title(); ?></h2>
<table>
<?php if ( ! empty( $post->post_excerpt ) ) : ?>
<tr>
<td>Description</td>
<td itemprop="description"><?php the_excerpt(); ?></td>
</tr>
<?php endif; ?>
<tr itemprop="exifData" itemscope itemtype="http://schema.org/PropertyValue">
<td itemprop="name" content="Camera" >Camera</td>
<td itemprop="value" content="<?php echo  $imgmeta['image_meta']['camera']; ?>"><?php echo  $imgmeta['image_meta']['camera']; ?></td>
</tr>
<tr itemprop="exifData" itemscope itemtype="http://schema.org/PropertyValue">
<td>Date</td>
<td itemprop="datePublished" content="<?php echo date("Y-m-d H:i:s", $imgmeta['image_meta']['created_timestamp']); ?>"><?php echo date("Y-m-d H:i:s", $imgmeta['image_meta']['created_timestamp']); ?></td>
</tr>
<tr itemprop="exifData" itemscope itemtype="http://schema.org/PropertyValue">
<td itemprop="name" content="MaxApertureValue" >Aperture</td>
<td itemprop="value" content="<?php echo  $imgmeta['image_meta']['aperture']; ?>"><?php echo  $imgmeta['image_meta']['aperture']; ?></td>
</tr>
<tr itemprop="exifData" itemscope itemtype="http://schema.org/PropertyValue">
<td itemprop="name" content="iso" >Iso</td>
<td itemprop="value" content="<?php echo  $imgmeta['image_meta']['iso']; ?>"><?php echo  $imgmeta['image_meta']['iso']; ?></td>
</tr>
<tr itemprop="exifData" itemscope itemtype="http://schema.org/PropertyValue">
<td itemprop="name" content="FNumber" >Focal Length</td>
<td itemprop="value" content="<?php echo  $imgmeta['image_meta']['focal_length']; ?>">f/<?php echo  $imgmeta['image_meta']['focal_length']; ?></td>
</tr>
<tr itemprop="exifData" itemscope itemtype="http://schema.org/PropertyValue">
<td itemprop="name" content="width" >Width</td>
<td itemprop="value" content="<?php echo  $original[1]; ?>"><?php echo  $original[1]; ?> px</td>
</tr>
<tr itemprop="exifData" itemscope itemtype="http://schema.org/PropertyValue">
<td itemprop="name" content="Height" >Height</td>
<td itemprop="value" content="<?php echo  $original[2]; ?>"><?php echo  $original[2]; ?> px</td>
</tr>
<tr>
<td>Copyright</td>
<td itemprop="author"><?php echo get_site_url(); ?></td>
</tr>		
</table>
</div>
</div>
</div>
</div>
</article>
<?php if(!isset($_GET['gallery'])): ?>
<nav id="pager-single">
<span class="pager-previous"><?php previous_image_link( false, __( 'Previous' , THEME_NAME ).' &rarr;' ); ?></span>
<span class="pager-next"><?php next_image_link( false, '&larr; '.__( 'Next' , THEME_NAME ) ); ?></span>
</nav>
<?php else: ?>
<nav id="pager-single">
<?php if(!empty($prev_href)): ?>
<span class="pager-next"><a href="<?php echo $prev_href; ?>"><?php echo '&larr; '.__( 'Previous' , THEME_NAME ) ?></a></span>
<?php endif; ?>
<?php if(!empty($next_href)): ?>
<span class="pager-previous"><a href="<?php echo $next_href; ?>"><?php echo __( 'Next' , THEME_NAME ).' &rarr;' ?></a></span>
<?php endif; ?>
</nav>
<?php endif ?>
<div class="clear"></div>
<?php if(!$hide_comments): ?>
<?php if ( comments_open() ) : ?>
<?php comments_template(); ?>
<?php endif; ?>
<?php endif; ?>
</div>
</main>
<div class="clear"></div>
</div>
<?php get_footer(); ?>