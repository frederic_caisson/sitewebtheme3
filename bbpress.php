<?php
/**
 * Template Name: With sidebar
 *
 *
 */
get_header(); 
$theme = My_Theme::get_instance();
$hide_comments = $theme->is_hide_comments();
?>
<div class="content-canvas">
<main id="main-content" class="primary fl">
<?php get_template_part('breadcrumb'); ?>
<?php the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry'); ?> itemscope itemtype="http://schema.org/BlogPosting" role="article" >
<?php get_template_part('header','entry'); ?>
<div class="entry-content e-content" itemprop="articleBody">
<?php 
	the_content();
?>
</div>
</article>
<?php if(!$hide_comments): ?>
<?php if ( comments_open() ) : ?>
<?php comments_template(); ?>
<?php endif; ?>
<?php endif; ?>
</main>
<div class="secondary fl">
<div id="right-sidebar" class="right-sidebar">
<?php dynamic_sidebar( 'sidebar-bbpress' )  ?>
</div>
</div>
<div class="clear"></div>
</div>
<?php get_footer(); ?>