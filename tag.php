<?php get_header(); ?>
<div class="content-canvas">
<?php get_template_part( 'feature' ); ?>
<?php get_sidebar('left'); ?>
<main id="main-content" role="main" class="primary fl">
<?php 
get_template_part('ad','top');
$theme = My_Theme::get_instance();
$theme->breadcrumb();  
$html_tag = $theme->get_tag_for('tag_category_title');
?>
<div class="h-feed category-feed">
<?php
echo "<$html_tag class='p-name category-title'>";
single_tag_title();
echo "</$html_tag>";
$tag_description = tag_description();
if(!empty($tag_description)):
?>
<div class="p-summary tag-description"><?php echo $tag_description ?></div>
<?php
endif;
$theme->get_loop_template();
?>
</div>
</main>
<?php get_sidebar('right'); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>