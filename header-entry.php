<?php
$theme = My_Theme::get_instance();
$html_tag = $theme->get_tag_for('tag_recent_post');

echo "<$html_tag class='p-name entry-title' itemprop='headline'>";
if(!is_attachment()):
	if($html_tag != 'h1'):
	    ?><a href="<?php the_permalink(); ?>" class="url u-url" title="<?php printf( esc_attr__( 'Permalink to %s', THEME_NAME ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark" ><?php 
	endif;
	the_title(); 
	if($html_tag != 'h1'):
	    ?></a><?php
	endif;
else:
	echo $theme::get_attachement_title();
endif;

echo "</$html_tag>";

edit_post_link( __( 'Edit', THEME_NAME ), '<span class="edit-link">', '</span>' );
?>
