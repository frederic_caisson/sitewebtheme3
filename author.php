<?php get_header(); ?>
<div class="content-canvas">
<?php get_template_part( 'feature' ); ?>
<?php get_sidebar('left'); ?>
<main id="main-content" class="primary fl">
<?php 
get_template_part('ad','top');
$theme = My_Theme::get_instance();
$theme->breadcrumb();  
$html_tag = $theme->get_tag_for('tag_category_title');
?>
<div class="h-feed category-feed">
<?php
echo "<$html_tag class='p-name category-title'>";
echo get_the_author();
echo "</$html_tag>";
?>
<?php
$theme->get_loop_template();
?>
<div class="visually-hidden u-uid" style="display:none;"><?php echo the_author_meta('ID'); ?></div>
</div>
</main>
<?php get_sidebar('right'); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>