<?php get_header(); 
$theme = My_Theme::get_instance();
$hide_comments = $theme->is_hide_comments();
?>
<div class="content-canvas">
<main id="main-content" class="fl full-page">
<?php get_template_part('breadcrumb'); ?>
<?php the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry'); ?>>
<?php get_template_part('header','entry'); ?>
<div class="entry-content e-content" itemprop="text">
<?php 
	the_content();
?>
</div>
</article>
<?php
$h = $theme->get_tag_for('tag_author');
?>
<?php   if (!$theme->is_hide_author()) :?>
<<?php echo $h ?> class="author-title"><i class="fa fa-info-circle"></i> <?php echo __('About the author', THEME_NAME); ?></<?php echo $h ?>>
<div class="h-card vcard author-about" itemprop="author" itemscope itemtype="https://schema.org/Person">
<?php 
$author_name = get_the_author_meta( 'display_name' );
$googleplus = get_the_author_meta( 'googleplus' );
?>
<meta itemprop="name" content="<?php echo $author_name?>">
<div class="u-photo author-avatar"><?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?></div>
<a class="p-name u-url fn url" href="<?php echo $googleplus; ?>"><?php echo $author_name?></a>
<div class="p-note author-description">
<?php echo get_the_author_meta( 'description' ); ?>
</div>
</div>
<?php   endif;?>
<?php if(!$hide_comments): ?>
<?php if ( comments_open() ) : ?>
<?php comments_template(); ?>
<?php endif; ?>
<?php endif; ?>
</main>
<div class="clear"></div>
</div>
<?php get_footer(); ?>