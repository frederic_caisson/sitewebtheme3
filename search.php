<?php get_header(); 
$theme = My_Theme::get_instance();
$search_query = get_search_query();
?>
<div class="content-canvas">
<?php get_sidebar('left'); ?>
<main id="main-content" role="main" class="primary fl">
<?php if ( have_posts() ) : ?>
<h1 class="entry-title"><?php printf( __( 'Search Results for: %s', THEME_NAME ),  '' ); echo $search_query; ?></h1>
<?php $theme->get_loop_template(); ?>
<?php else : ?>
<h1 class="entry-title"><?php echo __( 'Nothing Found', THEME_NAME ); ?></h1>
<div class="entry-content">
<p><i class="fa fa-exclamation-triangle"></i> <?php echo __( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', THEME_NAME ); ?></p>
</div>
<?php endif; ?>
</main>
<?php get_sidebar('right'); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>