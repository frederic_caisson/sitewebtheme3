<?php
$theme = My_Theme::get_instance();
$bottom_ad = $theme->get_option('bottom_ad');
if(!empty($bottom_ad)){
    echo "<div class='bottom-ad'>{$bottom_ad}</div>";
}

edit_post_link( __( 'Edit', THEME_NAME ), '<span class="edit-link">', '</span>' ); 
?>
<div class="footer-entry">
<div class="entry-meta">
<?php
$post_cats = get_the_category();
if($post_cats) {
$categories_list = '';
foreach ($post_cats as $cat) {
    $categories_list .= '<a rel="category" href="'.esc_url(get_category_link($cat->term_id)).'">'.$cat->name.'</a>, ';      
}
$categories_list = trim( $categories_list, ', '); 
echo '<div><i class="fa fa-archive"></i> '.$categories_list.'</div>';
}      

$post_tags = get_the_tags();
$tag_list = '';
if($post_tags) {

foreach ($post_tags as $tag) {
    $tag_list .= '<a rel="tag" href="'.esc_url(get_tag_link($tag->term_id)).'">#<span itemprop="keywords">'.$tag->name.'</span></a>, ';
}
$tag_list = trim( $tag_list, ', ');
echo '<div><i class="fa fa-tag"></i> '.$tag_list.'</div>';
}
?>
<?php if(!is_single()): ?>
</div>
<?php else:?>
<div class="dt-published published">
<time datetime="<?php the_time('Y-m-d'); ?>" class="date" itemprop="datePublished">
<i class="fa fa-clock-o"></i> <?php the_time( get_option('date_format') ); ?></time>
</div>
<time class="dt-updated updated visually-hidden" itemprop="dateModified" datetime="<?php the_modified_date('Y-m-d'); ?>">
<?php the_modified_date(); ?>
</time>
<div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
<?php   if ( has_post_thumbnail() ) : 
?>
<meta itemprop="url" content="<?php the_post_thumbnail_url(); ?>"/>
<meta itemprop="width" content="<?php echo My_Theme::THUMB_WIDTH; ?>">
<meta itemprop="height" content="<?php echo My_Theme::THUMB_HEIGHT; ?>">
<?php   endif; ?>
</div>
</div>
<?php 	if ($theme->is_show_related_posts()) : ?>
<div class="related-posts">
<?php $theme->related_posts();?>
</div>
<?php 	endif;?>
<?php  
if (!$theme->is_hide_author()) :
$h = $theme->get_tag_for('tag_author'); 
?>
<<?php echo $h ?> class="author-title"><i class="fa fa-info-circle"></i> <?php echo __('About the author', THEME_NAME); ?></<?php echo $h ?>>
<div class="h-card vcard author-about" itemprop="author" itemscope itemtype="https://schema.org/Person">
<?php 
$author_name = get_the_author_meta( 'display_name' );
$googleplus = get_the_author_meta( 'googleplus' );
?>
<meta itemprop="name" content="<?php echo $author_name?>">
<div class="u-photo author-avatar"><?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?></div>
<a class="p-name u-url fn url" href="<?php echo $googleplus; ?>"><?php echo $author_name?></a>
<div class="p-note author-description">
<?php echo get_the_author_meta( 'description' ); ?>
</div>
</div>
<?php   endif;?>
<?php endif; ?>
</div>