</div>       
<a href="#" class="scrollup">Scroll</a>
<div class="footer-canvas">
<footer class="footer">
<div class="widget-footer widget-area">
<?php if ( is_active_sidebar( 'footer-widget-area' ) ){
dynamic_sidebar( 'footer-widget-area' );                    
}?>
</div>
<div class="clear"></div>
<div class="copyright">
<?php 
$theme = My_Theme::get_instance();
$created_date = $theme->get_option('year');
$since = ' - ';
$thisYear = date("Y");
if(intval($created_date)>0) {
$since = ' '.$created_date.' - '.$thisYear. ' : ';
}
?>
&copy; <?php echo __( 'Copyright', THEME_NAME); echo $since; ?><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
<div class="copyright-html">
<?php echo $theme->get_option('copyright'); ?>
</div>
</div>
<?php
wp_nav_menu( array( 'depth' => 1, 'container_class' => 'footer-menu-canvas', 'theme_location' => 'footer') ); 
?>
</footer>
</div>
<?php
$bar_text = $theme->get_option('bar_text');
$bar_button_text = $theme->get_option('bar_button_text');
$bar_button_url = $theme->get_option('bar_button_url');
if(!empty($bar_text)):
?>
<div id="bar">
<?php 
echo $bar_text; 
if(!empty($bar_button_text)):
?>
<a href="<?php echo $bar_button_url ?>"><?php echo $bar_button_text ?></a>
<?php endif; ?>
</div>
<?php endif; ?>
<div id="cover"></div>
</div>
<?php 
wp_footer(); 
$feature_content_top = $theme->get_feature('top');
$feature_content_bottom = $theme->get_feature('bottom');
?>
<script>
var readmore = "<span><?php echo __('Continue reading', THEME_NAME); ?> <i class='fa fa-arrow-circle-o-right'></i></span>";
var feature_content_top = <?php echo json_encode($feature_content_top) ?>;
var feature_content_bottom = <?php echo json_encode($feature_content_bottom) ?>;
var $j = jQuery.noConflict();
$j(function(){$j('.mask').append(readmore);if(feature_content_top){$j('#feature-top').append(feature_content_top);$j('.feature-canvas-top').show()}if(feature_content_bottom){$j('#feature-bottom').append(feature_content_bottom);$j('.feature-canvas-bottom').show()}$j("#cover").hide()});
</script>
</body>
</html>