<?php 
include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
$theme = My_Theme::get_instance();
?>
<!DOCTYPE html>
<html <?php language_attributes();?> <?php echo $theme->get_microdata_schema();?>>
<head>
<?php 
get_template_part('meta'); 
wp_head();
$feature_background_top = $theme->get_feature_background('top');
$feature_background_bottom = $theme->get_feature_background('bottom');
?>
<style>
<?php 
echo '#cover{position:fixed;height:100%;width:100%;top:0;left:0;background:#fff;z-index:9999}';
if(!empty($feature_background_top)){
	echo " .feature-canvas-top{background:url($feature_background_top);background-repeat:repeat}";
}
if(!empty($feature_background_bottom)){
	echo " .feature-canvas-bottom{background:url($feature_background_bottom);background-repeat:repeat}";
}
?>
</style>
</head>
<?php get_template_part('startbody'); ?>