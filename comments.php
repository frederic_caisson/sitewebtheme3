<?php
if ( post_password_required() ){ return; }
$theme = My_Theme::get_instance();
$h = $theme->get_tag_for('tag_comment_title');
?>
<div id="comments-canvas">
<?php if ( have_comments() ) : ?>
<?php
$noComment = __('No Comments', THEME_NAME);
$oneComment = __('1 Comment', THEME_NAME);
$xComment = __('% Comments', THEME_NAME);
?>
<<?php echo $h ?> class="comments-title">
<i class="fa fa-comments"></i>
<?php 
comments_number($noComment, $oneComment, $xComment);
?>
</<?php echo $h ?>>
<ul class="comments" itemprop="comment" itemscope itemtype="http://schema.org/Comment">
<?php 
$args = array('type' => 'comment', 'callback' => array($theme, 'get_comments'));
wp_list_comments($args); 
?>
</ul>
<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
<nav id="comment-nav-below" class="navigation" role="navigation">
<h1 class="assistive-text section-heading"><?php echo __( 'Comment navigation', THEME_NAME ); ?></h1>
<div class="nav-previous"><?php previous_comments_link( '&larr; '.__( 'Older Comments', THEME_NAME ) ); ?></div>
<div class="nav-next"><?php next_comments_link( __( 'Newer Comments', THEME_NAME ).' &rarr;' ); ?></div>
</nav>
<?php endif; // check for comment navigation ?>
<?php
/* If there are no comments and comments are closed, let's leave a note.
* But we only want the note on posts and pages that had comments in the first place.
*/
if ( ! comments_open() && get_comments_number() ) : ?>
<p class="nocomments"><?php echo __( 'Comments are closed.' , THEME_NAME ); ?></p>
<?php endif; ?>
<?php endif; // have_comments() ?>
<?php comment_form(); ?>
</div>