<?php 
$theme = My_Theme::get_instance();
$m = wp_nav_menu( array( 'echo'=> false, 'container' => '','container_class' => 'collapse', 'theme_location' => 'primary', 'walker' => $theme->get_menu() ) );
if(!empty($m)):
?>
<div id="main-menu-canvas" class="main-menu-canvas">
<nav class="flat-mega-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
<label for="mobile-button"><i class="fa fa-bars"></i></label>
<input id="mobile-button" type="checkbox">
<?php echo $m; ?>
</nav>
</div>
<?php endif;?>