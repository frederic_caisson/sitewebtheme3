<div class="header-canvas">
<header class="header" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
<?php
$theme = My_Theme::get_instance();
$hide_search = $theme->get_option('hide_search');
if(empty($hide_search)){
	$hide_search = false;
} else {
	$hide_search = true;
}
$logo_image = get_header_image();

if($logo_image) :
?>
<div class="logo" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
<meta itemprop="url" content="<?php echo $logo_image; ?>">
<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
<img rel="logo" src="<?php echo $logo_image; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
</a>
</div>
<?php endif; ?>
<?php 
$hide_title = false;
$hide_sub_title = false;
?>
<div class="titles">
<?php 
$invisible_class = '';
if ($hide_title) { 
	$invisible_class = ' visually-hidden'; 
}
$html_tag = $theme->get_tag_for('tag_site_title');
//Site title
echo "<$html_tag class='site-title$invisible_class' itemprop='name' >";
?><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a><?php 
echo "</$html_tag>";

$blog_description = get_bloginfo('description');
//Site tag line
if(!empty($blog_description)){

$invisible_class = '';
if ($hide_sub_title) { 
	$invisible_class = ' visually-hidden'; 
}
$html_tag = $theme->get_tag_for('tag_blog_description');
echo "<$html_tag class='site-description$invisible_class' >";
bloginfo( 'description' );
echo "</$html_tag>";
} 
if(!$hide_search){
	$theme->print_search_input();
}
?>
</div>
<?php 
$show_social = false;
$socials = array('facebook_url' => '','twitter_url' => '','googleplus_url' => '','youtube_url' => '','instagram_url' => '','pinterest_url' => '');
foreach($socials as $key => $value){
	$socials[$key] = $theme->get_option($key);
}

foreach($socials as $key => $value){
	if(!empty($value)){
		$show_social = true;
		break;
	}
} 
if($show_social):
?>
<span class="social">
<?php
if (!empty($socials['facebook_url'])) {
?>
<a target="_blank" href="<?php echo $socials['facebook_url'] ?>" title="<?php echo __('Follow us on Facebook', THEME_NAME) ?>" ><i class="sprite sprite-facebook"></i></a>
<?php
}

if (!empty($socials['twitter_url'])) {
?>
<a target="_blank" href="<?php echo $socials['twitter_url'] ?>" title="<?php echo __('Follow us on Twitter', THEME_NAME) ?>" ><i class="sprite sprite-twitter"></i></a>
<?php
}

if (!empty($socials['googleplus_url'])) {
?>
<a target="_blank" href="<?php echo $socials['googleplus_url'] ?>" title="<?php echo __('Follow us on Google Plus', THEME_NAME) ?>" ><i class="sprite sprite-googleplus"></i></a>
<?php
}


if (!empty($socials['youtube_url'])) {
?>
<a target="_blank" href="<?php echo $socials['youtube_url'] ?>" title="<?php echo __('Follow us on Youtube', THEME_NAME) ?>" ><i class="sprite sprite-youtube"></i></a>
<?php
}

if (!empty($socials['instagram_url'])) {
?>
<a target="_blank" href="<?php echo $socials['instagram_url'] ?>" title="<?php echo __('Follow us on Instagram', THEME_NAME) ?>" ><i class="sprite sprite-instagram"></i></a>
<?php
}

if (!empty($socials['pinterest_url'])) {
?>
<a target="_blank" href="<?php echo $socials['pinterest_url'] ?>" title="<?php echo __('Follow us on Pinterest', THEME_NAME) ?>" ><i class="sprite sprite-pinterest"></i></a>
<?php
}
?>
</span>
<?php
endif;
?>
</header>
</div>