<?php
if(is_paged()):
?>
<div class="h-feed">
</div>
<?php
else:
	$theme = My_Theme::get_instance();
	$cat_ids = $theme->get_selected_categories();
	$limit = $theme::POSTS_LIMIT;
	foreach($cat_ids as $cat_id):
		$recent = new WP_Query("cat=$cat_id&showposts=$limit"); 
		$category_name = get_cat_name( $cat_id );
		$html_tag = $theme->get_tag_for('tag_category_title');
		$cat_link = get_category_link( $cat_id );
?>
<div class="h-feed category-feed">
<?php
		echo "<$html_tag class='p-name category-title'><a href='$cat_link'>$category_name</a></$html_tag>";
?>
<?php
		$count = 0;
		while($recent->have_posts()):
			$count++;
			$recent->the_post();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry hentry article-feed'); ?> <?php echo $post_style; ?> itemscope itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
<?php 		if ( has_post_thumbnail() ) : ?>
<a href="<?php the_permalink(); ?>" class="article-feed-thumb">
<?php 
				$thumb_attr = array(
					'class' => "u-photo entry-thumbnail",
					'alt'   => trim( strip_tags( get_the_title() ) ),
				);
				the_post_thumbnail( 'thumb-fb', $thumb_attr ); 
?><div class="mask"></div></a>
<?php 		endif;?>
<?php 		get_template_part('header','entry'); ?>
<p class="entry-summary p-summary"><?php echo get_the_excerpt();?></p>
<div class="visually-hidden u-uid"><?php the_ID(); ?></div>
<?php get_template_part('invisible','info'); ?>
</article>
<div class="clear"></div>
<?php 
		endwhile; 
		if($count > 0):
?>
<div class="show-more center w50"><a href="<?php echo $cat_link; ?>"><?php echo __('More articles', THEME_NAME); ?>&nbsp;&nbsp;<i class="fa fa-bars"></i></a></div>
<?php 	endif; ?>
<div class="visually-hidden u-uid"><?php echo $cat_id ?></div>
</div>
<?php
	endforeach;
endif;
?>
