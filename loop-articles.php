<?php 
$thumbnail_class = 'entry-thumbnail';
get_template_part('pager');
?>
<?php
while ( have_posts() ):
	the_post(); 
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry hentry article-feed'); ?> <?php echo $post_style; ?> itemscope itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
<?php if ( has_post_thumbnail() ): ?>
<a href="<?php the_permalink(); ?>" class="article-feed-thumb txtcenter" itemprop="url">
<?php 
	$thumb_attr = array(
	'class' => "u-photo entry-thumbnail",
	'alt'   => trim( strip_tags( get_the_title() ) ),
	'itemprop'=> "thumbnailUrl"
	);
	the_post_thumbnail( 'thumb-fb', $thumb_attr ); 
?>
<div class="mask"></div>
</a>
<?php endif; ?>
<?php get_template_part('header','entry'); ?>
<p class="entry-summary p-summary" itemprop="text"><?php echo get_the_excerpt();?></p>
<div class="visually-hidden u-uid" style="display:none;"><?php the_ID(); ?></div>
<?php get_template_part('invisible','info'); ?>
</article>
<div class="clear"></div>
<?php 
endwhile;
?>
<?php
get_template_part( 'pager'); 
?>