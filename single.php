<?php get_header();
$theme = My_Theme::get_instance();
$hide_comments = $theme->is_hide_comments();
?>
<div class="content-canvas">
<?php get_sidebar('left'); ?>
<main id="main-content" class="primary fl">
<?php get_template_part('ad','top'); ?>
<?php get_template_part('feature') ?>
<?php get_template_part('breadcrumb'); ?>
<?php get_template_part('pager','single'); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry single'); ?>>
<?php get_template_part('header','entry'); ?>
<div class="entry-content e-content" itemprop="articleBody" >
    <?php the_content(); ?>
</div>
<?php get_template_part('feature2') ?>
<?php get_template_part('footer','entry'); ?>
<?php if(!$hide_comments): ?>
    <?php if ( comments_open() ) : ?>
        <?php comments_template(); ?>
    <?php endif; ?>
<?php endif; ?>
<link itemprop="mainEntityOfPage" href="<?php echo site_url(); ?>">
</article>
<?php endwhile; ?>
<?php get_template_part('pager','single'); ?>
</main>
<?php get_sidebar('right'); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>