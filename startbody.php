<?php 
$theme = My_Theme::get_instance();
?>
<body <?php body_class(); ?>>
<div class="container">
<?php
// Logo & Title & Social
get_template_part( 'branding'); 
// Navigation Bar
get_template_part( 'nav' ); 
?>
<div class="line">