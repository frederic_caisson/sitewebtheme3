sitewebtheme
============

A flexible wordpress theme :
- Highly customizable via adminstration panel
- Responsive
- Multi-columns (1 column, 2 columns, 3 columns)