<title><?php
	$theme = My_Theme::get_instance();
	if(!is_attachment()){
	    if(is_plugin_active('wordpress-seo/wp-seo.php')) {
			wp_title( '-', true, 'right' );
		} else {
			global $page, $paged;
			wp_title( '-', true, 'right' );
			bloginfo( 'name' );
			if(is_home())
			{
				$site_description = get_bloginfo( 'description', 'display' );
				if ( $site_description && ( is_home() || is_front_page() ) )
					echo " : $site_description";
			}

			if ( $paged >= 2 || $page >= 2 ){
				echo ' - ' . sprintf( __( 'Page', THEME_NAME ).' %s', max( $paged, $page ) );
			}
		}
	} else {
		echo $theme->get_attachement_title();
	}
?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="initial-scale=1.0">
<link rel="shortcut icon" href="<?php echo site_url(); ?>/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo site_url(); ?>/apple-touch-icon.png"><!--60X60-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if(is_attachment()):
$original = wp_get_attachment_image_src( $post->ID, 'full');
?>
<meta property="og:image" content="<?php echo $original[0] ?>" />
<?php endif;?>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->