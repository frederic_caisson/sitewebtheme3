<?php 
$theme = My_Theme::get_instance();
$layout = $theme->get_option('site_layout');
$layout = intval($layout);

if($layout == 3 || $layout == 4):
?>
<div class="secondary fl">
<div id="right-sidebar" class="right-sidebar">
<?php dynamic_sidebar( 'right-sidebar' )  ?>
</div>
</div>
<?php endif ?>