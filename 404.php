<?php get_header(); ?>
<?php the_post(); ?>
<div class="content-canvas">
	<div class="content item full-width">
		<article <?php post_class(); ?>>
		<header class="entry-header">
		<h1 class="entry-title"><?php echo __( 'This is somewhat embarrassing, isn&rsquo;t it?', THEME_NAME ); ?></h1>
		</header>
		<div class="entry-content">
		<p><?php echo __( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', THEME_NAME ); ?>&nbsp;<?php echo __( 'Perhaps searching, or one of the links below, can help.', THEME_NAME ); ?></p>
		<?php get_search_form(); ?>
		<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
		<div class="widget">
		<h2 class="widgettitle"><?php echo __( 'Most Used Categories', THEME_NAME ); ?></h2>
		<ul>
		<?php wp_list_categories( array( 'orderby' => 'count', 'order' => 'DESC', 'show_count' => 'TRUE', 'title_li' => '', 'number' => '10' ) ); ?>
		</ul>
		</div>
		<?php the_widget( 'WP_Widget_Archives', 'dropdown=1' ); ?>
		<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>
		</div>
		</article>
	</div>
<div class="clear"></div>
</div>
<?php get_footer(); ?>