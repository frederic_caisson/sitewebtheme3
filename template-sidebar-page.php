<?php
/**
 * Template Name: With sidebar
 *
 *
 */
get_header(); 
$theme = My_Theme::get_instance();
$hide_comments = $theme->is_hide_comments();
?>
<div class="content-canvas">
<?php get_sidebar('left'); ?>
<main id="main-content" class="primary fl">
<?php get_template_part('breadcrumb'); ?>
<?php the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('h-entry'); ?>>
<?php get_template_part('header','entry'); ?>
<div class="entry-content e-content" itemprop="text">
<?php 
	the_content();
?>
</div>
</article>
<?php if(!$hide_comments): ?>
<?php if ( comments_open() ) : ?>
<?php comments_template(); ?>
<?php endif; ?>
<?php endif; ?>
</main>
<?php get_sidebar('right'); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>
