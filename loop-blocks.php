<?php 
$theme = My_Theme::get_instance();
$list_ad = $theme->get_option('list_ad');
$ads = array();
$pos = -1;
if(!empty($list_ad)){
	$pos = rand ( 0 , 1 );
	$ads[$pos] = $list_ad;
	$ads[$pos+2] = $list_ad;
}

$count = 0;
while ( have_posts() ) : the_post(); 

	if(array_key_exists($count,$ads)){
		echo "<div class='fl article-ad-base'>{$ads[$count]}</div>";
	}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('fl article-ad article-ad-base'); ?> <?php echo $post_style; ?> itemscope itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
<?php get_template_part('header','entry'); ?>
<div class="flex-container">
<?php if ( has_post_thumbnail() ) : 
	$thumb_attr = array(
	'class' => "u-photo entry-thumbnail",
	'alt'   => trim( strip_tags( get_the_title() ) ),
	);
?>
<div class="w150p">
<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail', $thumb_attr );?></a>
</div>
<?php endif;?>
<div class="pls prs flex-item-fluid">
<?php echo $theme->the_excerpt_max_charlength(100); ?>
</div>
</div>
<div class="visually-hidden u-uid" style="display:none;"><?php the_ID(); ?></div>
<?php get_template_part('invisible','info'); ?>
</article>
<?php 
$count++;
endwhile; 
?>
<div class="clear"></div>
<?php
get_template_part( 'pager'); 
?>