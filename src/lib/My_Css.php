<?php
class My_Css {

	private $css_libs;
	private $plugin_dir;
	private $all_css_dir;
	private $css_file;
	private $options;

	public function __construct(){
		$this->defaults = array('top_ad' => '',
								'bottom_ad' => '',
								'list_ad' => '',
								'facebook_url' => '',
								'twitter_url' => '',
								'googleplus_url' => '',
								'youtube_url' => '',
								'instagram_url' => '',
								'pinterest_url' => '',
								'year' => '',
								'copyright' => '',
								'site_width' => 1024,
								'sidebar_left_width' => 200,
								'sidebar_right_width' => 200,
								'site_layout' => 1,
								'articles_organization' => 1,
								'color_site_title' => '#000000',
								'color_subtitle' => '#333333',
								'color_header_background' => '',
								'color_link' => '#0000FF',
								'color_link_hover' => '#FF0000',
								'color_menu_background' => '#E6E6E6',
								'color_content_background' => '',
								'color_sidebar_background' => '',
								'color_sidebar_title_background' => '#CCCCCC',
								'color_sidebar_title_font' => '#333333',
								'color_footer_background' => '#E6E6E6',
								'color_footer_font' => '#333333',
								'color_sidebar_title_background' => '#CCCCCC',
								'color_primary_button_background' => '#619AF3',
								'color_primary_button_background_shadow' => '#5083EC',
								'color_primary_button_font' => '#FFFFFF',
								'color_action_button_background' => '#FFCC36',
								'color_action_button_background_shadow' => '#F4BD0E',
								'color_action_button_font' => '#FFFFFF',
								'color_forum_canvas' => '#CCCCCC',
								'color_category_title_background' => '#CCCCCC',
								'color_category_title_font' => '#333333',
								);
		$this->css_libs = array('knacss.css','font-awesome.min.css');
		$this->plugin_dir = get_template_directory();
		$this->all_css_dir = $this->plugin_dir.'/css/';
		$this->css_file = $this->plugin_dir.'/'.MY_CSS_FILE_NAME;
	}

	public function __destruct(){
    	$this->css_libs = null;
    	$this->plugin_dir = null;
    	$this->all_css_dir = null;
		$this->css_file = null;
		$this->options = null;
    }

	public function get_defaults(){
		return $this->defaults;
	}

	public function lighten_darken_color($hex, $percent) {
		// Work out if hash given
		$hash = '';
		if (stristr($hex,'#')) {
			$hex = str_replace('#','',$hex);
			$hash = '#';
		}
		/// HEX TO RGB
		$rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
		//// CALCULATE 
		for ($i=0; $i<3; $i++) {
			// See if brighter or darker
			if ($percent > 0) {
				// Lighter
				$rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
			} else {
				// Darker
				$positivePercent = $percent - ($percent*2);
				$rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1-$positivePercent));
			}
			// In case rounding up causes us to go to 256
			if ($rgb[$i] > 255) {
				$rgb[$i] = 255;
			}
		}
		//// RBG to Hex
		$hex = '';
		for($i=0; $i < 3; $i++) {
			// Convert the decimal digit to hex
			$hexDigit = dechex($rgb[$i]);
			// Add a leading zero if necessary
			if(strlen($hexDigit) == 1) {
			$hexDigit = "0" . $hexDigit;
			}
			// Append to the hex string
			$hex .= $hexDigit;
		}
		return $hash.$hex;
	}

	public function create(){
		
		$path = $this->css_file;
		
		$file = fopen($path,"w") or die("Unable to open file!");

		if ($file !== false){
			foreach($this->css_libs as $css_lib){
				$src = $this->all_css_dir.$css_lib;
				$content = file_get_contents($src);
				fwrite($file, $content);
				$content = null;
			}
			$this->add_options($file);

			fclose($file);
		}
	}

	public function set_options($options){
		$this->options = $options;
	}

	public function add_options($file){
		$site_layout = intval($this->options['site_layout']);
		$font_family = 'Impact';

		$color_link = $this->options['color_link'];
		$color_link_hover = $this->options['color_link_hover'];
		$color_h3 = $this->lighten_darken_color($color_link,0.5);

		if(!empty($this->options['color_header_background'])){
			$color_header_background = $this->options['color_header_background'];
		} else {
			$color_header_background = 'tranparent';
		}

		$color_site_title = $this->options['color_site_title'];
		$color_subtitle = $this->options['color_subtitle'];

		if(!empty($this->options['color_content_background'])){
			$color_content_background = $this->options['color_content_background'];
		} else {
			$color_content_background = 'tranparent';
		}

		if(!empty($this->options['color_footer_background'])){
			$color_footer_background = $this->options['color_footer_background'];
		} else {
			$color_footer_background = 'tranparent';
		}

		$color_footer_font = $this->options['color_footer_font'];

		if(!empty($this->options['color_menu_background'])){
			$color_menu_background = $this->options['color_menu_background'];
		} else {
			$color_menu_background = 'tranparent';
		}
		
		if(!empty($this->options['color_primary_button_background'])){
			$color_primary_button_background = $this->options['color_primary_button_background'];
		} else {
			$color_primary_button_background = 'tranparent';
		}

		if(!empty($this->options['color_primary_button_background_shadow'])){
			$color_primary_button_background_shadow = $this->options['color_primary_button_background_shadow'];
		} else {
			$color_primary_button_background_shadow = 'tranparent';
		}

		$color_primary_button_font = $this->options['color_primary_button_font'];

		if(!empty($this->options['color_action_button_background'])){
			$color_action_button_background = $this->options['color_action_button_background'];
		} else {
			$color_action_button_background = 'tranparent';
		}

		if(!empty($this->options['color_action_button_background_shadow'])){
			$color_action_button_background_shadow = $this->options['color_action_button_background_shadow'];
		} else {
			$color_action_button_background_shadow = 'tranparent';	
		}

		$color_action_button_font = $this->options['color_action_button_font'];

		if(!empty($this->options['color_sidebar_background'])){
			$color_sidebar_background = $this->options['color_sidebar_background'];
		} else {
			$color_sidebar_background = 'tranparent';
		}

		if(!empty($this->options['color_sidebar_title_background'])){
			$color_sidebar_title_background = $this->options['color_sidebar_title_background'];

		} else {
			$color_sidebar_title_background = 'tranparent';
		}

		$color_sidebar_title_font = $this->options['color_sidebar_title_font'];

		if(!empty($this->options['color_category_title_background'])){
			$color_category_title_background = $this->options['color_category_title_background'];
		} else {
			$color_category_title_background = 'tranparent';
		}

		$color_category_title_font = $this->options['color_category_title_font'];
		
		$site_width = $this->options['site_width'];
		$side_left_width = 0;
		$side_right_width = 0;
		switch($site_layout){
	        case 2:
	            $sidebar_left_width = $this->options['sidebar_left_width'];
	        break;
	        case 3:
	            $sidebar_right_width = $this->options['sidebar_right_width'];
	        break;
	        case 4:
	            $sidebar_left_width = $this->options['sidebar_left_width'];
	            $sidebar_right_width = $this->options['sidebar_right_width'];
	        break;
    	}
    	
		$primary_width = $site_width - $sidebar_left_width - $sidebar_right_width;

		$site_width = $site_width.'px';
		$sidebar_left_width = $sidebar_left_width.'px';
		$sidebar_right_width = $sidebar_right_width.'px';
		$primary_width = $primary_width.'px';

		$color_quote_background = '#DEE4F4';
        $color_quote_font = '#000';

        $color_bar_background = '#000';
        $color_bar_font = '#000';
        $color_bar_button = '#000';

        $color_forum_topic_background = $this->lighten_darken_color($color_quote_background,0.8);
        $color_forum_topic_background2 = $this->lighten_darken_color($color_quote_background,0.5);

		$content = "a:link,a:active,a:visited{color:$color_link}";
		$content .= "a:hover{color:$color_link_hover}";
		$content .= "tbody tr:nth-child(odd){background:rgba(200,200,200,0.4)}";
		$content .= "blockquote{padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:35px;";
		$content .= "background:$color_quote_background;";
		$content .= "color:$color_quote_font;font-weight:bold}";
		$content .= 'blockquote:before{content:"\f10d";font-family:FontAwesome;float:left;opacity:0.1;margin:10px;font-size:20px}';
		$content .= ".widgets-list-layout li{width:200px}";
		$content .= ".clear{clear:both}";
		$content .= ".edit-link{padding:2px;background:yellow}";
		$content .= ".fixed-top{position:fixed;top:0;z-index:999}";
		$content .= ".container .lteie8{width:$site_width}";
		$content .= ".social4i{margin-top:10px}";
		$content .= ".header-canvas{width:100%;float:left;background-color:$color_header_background}";
		$content .= ".header{margin:0 auto;max-width:$site_width}";
		$content .= ".logo{float:left;margin-top:5px;margin-bottom:5px}";
		$content .= ".titles{float:left;margin-right:0px;margin-left:10px;margin-top:10px;margin-bottom:15px}";
		$content .= ".social{float:right;padding:20px}";
		$content .= ".social a{text-decoration:none;margin-left:5px}";
		$content .= ".social i{display:inline-block;cursor:pointer}";
		$content .= ".social a:hover{opacity:0.8}";
		$content .= ".site-title{font-family:$font_family;margin:0;padding:0;line-height:1.5em;font-size:40px;font-weight:bold;color:$color_site_title;display:block}";
		$content .= ".site-title a{text-decoration:none;color:$color_site_title}";
		$content .= ".site-description{margin:0;padding:0;line-height:1em;font-size:18px;font-weight:bold;color:$color_subtitle;display:block}";
		$content .= ".content-canvas{margin:0 auto;max-width:$site_width;background-color:$color_content_background}";
		$content .= ".primary{max-width:$primary_width;width:$primary_width;padding:10px}";
		$content .= ".full-page{max-width:$site_width;width:$site_width;padding:10px}";
		$content .= ".feature-canvas-top,.feature-canvas-bottom{display:none;clear:both}";
		$content .= ".feature-category{padding:10px}";
		$content .= ".feature-single{padding:10px;margin-bottom:10px}";
		$content .= ".footer-canvas{padding-bottom:48px;background-color:$color_footer_background;background-color:$color_footer_background}";
		$content .= ".footer{color:$color_footer_font;padding:15px}";
		$content .= ".footer .widget-title{margin:0;margin-bottom:10px;padding:0;font-size:19px;font-weight:bold}";
		$content .= ".footer .widget-area{display:inline-block}";
		$content .= ".footer-widget{max-width:300px;margin:10px;float:left}";
		$content .= ".footer-widget ul{list-style-type:none;margin:0;padding:0}";
		$content .= ".footer-widget ul li{line-height:2em}";
		$content .= ".copyright{text-align:center}";
		$content .= ".main-menu-canvas{width:100%;float:left;background-color:$color_menu_background}";
		$content .= ".footer-menu-canvas{font-size:x-small}";
		$content .= ".footer-menu-canvas ul{text-align:center;list-style-type:none;margin:0;padding:0}";
		$content .= ".footer-menu-canvas ul li{list-style-type:none;display:inline;margin:10px}";
		$content .= ".pager{padding:10px;display:block;overflow:hidden;padding-bottom:10px}";
		$content .= ".pager ul{margin:0;padding:0;list-style-type:none}";
		$content .= ".pager ul li{margin-right:5px;display:inline;line-height:2.5em}";
		$content .= ".pager ul li a{padding-top:6px;padding-bottom:5px;padding-left:15px;padding-right:15px;border-radius:5px;background-color:$color_primary_button_background;box-shadow:0px 3px 0px 0px $color_primary_button_background_shadow;text-decoration:none;color:$color_primary_button_font}";
		$content .= ".pager ul li a:hover{padding-top:7px;padding-bottom:4px;opacity:0.9;box-shadow:0px 3px 0px 0px $color_primary_button_background;color:$color_primary_button_font}";
		$content .=".pager ul li .current{padding-left:15px;padding-right:15px;border-radius:5px;background-color:$color_primary_button_background;text-decoration:none;color:$color_primary_button_font;padding-top:7px;padding-bottom:4px;opacity:0.9;box-shadow:0px 3px 0px 0px $color_primary_button_background}";
		$content .=".pager-previous a{padding-top:6px;padding-bottom:5px;padding-left:15px;padding-right:15px;border-radius:5px;background-color:$color_primary_button_background;box-shadow:0px 3px 0px 0px $color_primary_button_background_shadow;text-decoration:none;color:$color_primary_button_font;display:block;margin-bottom:5px}";
		$content .=".pager-next a{padding-top:6px;padding-bottom:5px;padding-left:15px;padding-right:15px;border-radius:5px;background-color:$color_primary_button_background;box-shadow: 0px 3px 0px 0px $color_primary_button_background_shadow;text-decoration:none;color:$color_primary_button_font;display:block;margin-bottom:5px}";
		$content .=".pager-previous a:hover,.pager-next a:hover{padding-top:7px;padding-bottom:4px;opacity:0.9;box-shadow:0px 3px 0px 0px $color_primary_button_background;color:$color_primary_button_font}";
		$content .=".pager-previous{margin-top:1px;float:right;text-align:right}";
		$content .=".pager-next{margin-top:1px;float:left}";
		$content .=".pager-single{display:block;overflow:hidden;padding-bottom:5px}";
		$content .=".entry-title{font-size:30px;font-weight:bold;margin:0;padding:0}";
		$content .=".entry-title a{text-decoration:none}";
		$content .=".entry-exerpt{text-align:justify;text-justify:inter-word}";
		$content .=".entry-content{margin:10px}";
		$content .=".entry-content p{line-height:1.8em}";
		$content .=".entry-content li{line-height:1.8em}";
		$content .=".entry-content h1,.action-form h1{font-size:28px}";
		$content .=".entry-content h2,.action-form h2{font-size:24px;color:#333}";
		$content .=".entry-content h3,.action-form h3{font-size:20px;color:$color_h3}";
		$content .=".entry-content h4,.action-form h4{font-size:18px;font-style:italic;color:#999}";
		$content .=".entry-content h5{font-size:14px;font-style:italic;color:#333}";
		$content .=".entry-content h6{margin:0;font-size:12px;font-weight:bold}";
		$content .=".entry-content code{display:block;background-color:#333;color:#fff;padding:10px;width:100%;line-height:2em}";
		$content .=".entry-meta{font-size:smaller;padding:5px;background:#eee;border-left:solid 3px $color_link;margin-bottom:10px}";
		$content .=".author-about{background:#eee;border-bottom:solid 2px #ccc;margin-bottom:10px;padding:10px}";
		$content .=".author-title{padding:5px;margin:0;font-size:16px;color:#333;background-color:$color_menu_background}";
		$content .=".author-avatar{display:inline}";
		$content .=".author-avatar img{border-radius:36px}";
		$content .=".author-description{display:block}";
		$content .=".category-title{margin:0;margin-bottom:10px;padding-left:5px;padding-right:5px;background-color:$color_category_title_background;font-size:24px}";
		$content .='.category-title:after{content:" » ";'."color:$color_link}";
		$content .=".category-title a{color:$color_category_title_font;text-decoration:none}";
		$content .=".show-more{margin-bottom:15px}";
		$content .=".show-more a{text-align:center;padding-top:6px;padding-bottom:5px;padding-left:15px;padding-right:15px;border-radius:5px;background-color:$color_primary_button_background;box-shadow:0px 3px 0px 0px $color_primary_button_background_shadow;text-decoration:none;color:$color_primary_button_font;display:block}";
        $content .=".show-more a:hover{padding-top:7px;padding-bottom:4px;opacity:0.9;box-shadow:0px 3px 0px 0px $color_primary_button_background;color:$color_primary_button_font}";
		$content .=".action-button{border:0;padding-top:6px;padding-bottom:3px;padding-left:10px;padding-right:10px;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;background-color:$color_action_button_background;box-shadow:0px 3px 0px 0px $color_action_button_background_shadow;text-decoration:none;color:$color_action_button_font;font-size:18px;font-weight:bold}";
		$content .="a.action-button:link{color:$color_action_button_font}";
		$content .="a.action-button:active{color:$color_action_button_font}";
		$content .="a.action-button:visited{color:$color_action_button_font}";
		$content .=".action-button:hover{cursor:pointer;opacity:0.9;padding-top:7px;padding-bottom:2px;box-shadow: 0px 2px 0px 0px $color_action_button_background;color:$color_action_button_font}";
		$content .=".article-ad-base{width:336px;height:280px;margin:5px;word-wrap:break-word}";
		$content .=".article-ad{padding:5px;border-bottom:1px solid #ccc}";
		$content .=".article-ad .entry-title{text-align:center;font-weight:normal}";
		$content .=".article-ad .entry-title a:hover{text-decoration:underline}";
		$content .=".article-feed{margin-left:10px;background:#eee;padding:10px;border-bottom:solid 2px #ccc;margin-bottom:15px}";
		$content .=".article-feed .entry-title{font-weight:bold;font-size:24px}";
		$content .=".article-feed p{margin:0;margin-top:5px;padding:0}";
		$content .=".article-feed-thumb{display:table-cell;max-width:484px;max-height:252px;overflow:hidden;position:relative}";
		$content .=".article-feed-thumb img{width:484px;height:252px}";
		$content .=".article-feed-thumb:hover img{transform:scale(1.5);-o-transform:scale(1.5);-moz-transform:scale(1.5)}";
		$content .='.article-feed-thumb .mask{-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:alpha(opacity=0);opacity:0;background-color:#000;width:484px;height:252px;overflow:hidden;display:block;position:absolute;top:0;left:0}';
		$content .='.article-feed-thumb .mask:hover{-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0.7)";filter:alpha(opacity=0.7);opacity:0.7}';
		$content .='.article-feed-thumb .mask span{font-size:28px;color:#fff;text-align:center;margin:auto 0;display:block;vertical-align:middle;line-height:252px}';
		$content .='.related-title{font-size:16px;color:#333;margin:0;padding:0}';
		$content .='.related-posts ul{list-style-type:none;margin:0;padding:0}';
		$content .='.related-posts ul li{vertical-align:top;display:inline-block;width:150px;margin:10px;text-align:center}';
		$content .=".breadcrumb{font-size:x-small;color:#ccc;padding:5px;background:#eee;border-left:solid 3px $color_link;margin-bottom:10px}";
		$content .=".breadcrumb div{display:inline}";
		$content .=".wp-caption{padding:.5em;text-align:center}";
		$content .=".wp-caption img{margin:.25em}";
		$content .=".wp-caption .wp-caption-text{margin:5px;color:#666;font-style:italic}";
		$content .="#wp-calendar #next {text-align:right}";
		$content .="input[type=hidden]{margin:0;padding:0}";
		$content .="label{display:inline-block}";
		$content .="input[name=s],input[type=email],input[type=password],input[type=text],input[type=url],textarea{background:#f1f1f1;border:1px solid #ccc;-moz-box-shadow:1px 1px 0 0 #E9E9E9;-webkit-box-shadow:1px 1px 0 0 #E9E9E9;box-shadow:1px 1px 0 0 #E9E9E9;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;padding:5px;margin:5px;color:#666;font-size:14px}";
		$content .=".searchform2 {color:#666}";
		$content .=".searchform2 input[name=s]{border:0px;background:transparent;margin:0px;margin-top:5px}";
		$content .="input[type=submit]:hover{cursor:pointer}";
		$content .="input[name=submit]{padding:20px}";
		$content .="select{background:#f1f1f1;padding:5px;font-size:14px;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;-webkit-appearance:none;-moz-box-shadow:1px 1px 0 0 #E9E9E9;-webkit-box-shadow:1px 1px 0 0 #E9E9E9;box-shadow:1px 1px 0 0 #E9E9E9;border:1px solid #ccc;color:#666}";
		$content .="textarea:focus,input[type=text]:focus,input[type=url]:focus,input[type=email]:focus,input[type=password]:focus,input[name=s]:focus{outline:none;border:1px solid #7bc1f7;box-shadow:0px 0px 8px #7bc1f7;-moz-box-shadow:0px 0px 8px #7bc1f7;-webkit-box-shadow:0px 0px 8px #7bc1f7}";
		$content .= "#comments-canvas{margin-bottom:10px}";
		$content .="#commentform input[type=submit],.searchform input[type=submit]{border:0;padding-top:4px;padding-bottom:2px;padding-left:8px;padding-right:8px;-webkit-border-radius:18px;-moz-border-radius:18px;border-radius:18px;background-color:$color_primary_button_background;box-shadow:0px 3px 0px 0px $color_primary_button_background_shadow;text-decoration:none;color:$color_primary_button_font;font-size:15px}";
		$content .="#commentform input[type=submit]:hover,.searchform input[type=submit]:hover{cursor:pointer;opacity:0.8;padding-top:5px;padding-bottom:1px;box-shadow:0px 2px 0px 0px $color_primary_button_background}";
		$content .= ".comments{margin:0;padding:0}";
		$content .= ".comments-title{padding:5px;margin:0;font-size:16px;color:#333;background-color:$color_menu_background}";
		$content .= ".comment{display:block;list-style-type:none;padding:0;margin-right:0;margin-left:0;margin-top:5px;margin-bottom:5px}";
		$content .= ".comment-author-name{font-weight:bold;margin-right:5px}";
		$content .= ".comment-time{font-style:italic;font-size:80%;opacity:0.6}";
		$content .= ".comment-avatar{display:inline;margin-right:5px}";
		$content .= ".comment-avatar img{border-radius:24px}";
		$content .= ".comment-meta{display:inline}";
		$content .= ".comment-triangle{margin-top:-18px;margin-bottom:10px}";
		$content .= '.comment-triangle:before{content:"";'."border-bottom:30px solid $color_quote_background;border-top-color:inherit;border-right:25px solid transparent;border-left:25px solid transparent}";
		$content .= ".comment-text{background:$color_quote_background;padding:10px}";
		$content .= ".comment-buttons{text-align:right;clear:both;display:block}";
		$content .= "#respond .comment-reply-title{font-size:16px;color:#333}";
		$content .= "#respond{padding:10px;background-color:$color_quote_background}";
		$content .= ".left-sidebar{width:$sidebar_left_width;max-width:$sidebar_left_width;background:$color_sidebar_background}";
		$content .= ".right-sidebar{width:$sidebar_right_width;max-width:$sidebar_right_width;background:$color_sidebar_background}";
		$content .= ".right-sidebar .widget{margin-bottom:10px;background:$color_sidebar_background}";
		$content .= ".left-sidebar .widget{margin-bottom:10px;background:$color_sidebar_background}";
		$content .= ".right-sidebar .widget-title{margin:0;padding-left:10px;padding-right:10px;font-size:19px;font-weight:bold;color:$color_sidebar_title_font;background:$color_sidebar_title_background}";
		$content .= ".left-sidebar .widget-title{margin:0;padding-left:10px;padding-right:10px;font-size:19px;font-weight:bold;color:$color_sidebar_title_font;background:$color_sidebar_title_background}";
		$content .= ".widget ul{list-style-type:none}";
		$content .= ".widget ul li{line-height:2em}";
		$content .= ".widget_calendar caption{color:$color_sidebar_title_font;font-size:19px;font-weight:bold}";
		$content .= ".tagcloud{padding:10px}";
		$content .= ".tagcloud a{line-height:2em}";
		$content .= ".screen-reader-text{margin-left:10px}";
		$content .= ".scrollup{width:48px;height:48px;position:fixed;bottom:10px;right:10px;display:none;text-indent:-9999px;background:url('images/top.png') no-repeat}";
		$content .= "#bar{box-shadow:1px 0px 6px #666;position:fixed;top:0px;height:36px;width:100%;text-align:center;padding:8px;z-index:1000;background-color:$color_bar_background;color:$color_bar_font}";
		$content .= "#bar a{text-decoration:none;border-radius:5px;margin:6px;padding:6px;background-color:$color_bar_button;color:$color_bar_font}";
		$content .= "#bar a:hover{opacity:0.8}";
		$content .= "#bbpress-forums .info{display:none}";
		$content .= "#bbpress-forums .bbp-breadcrumb{background:#eee;border-left:solid 3px $color_link;margin-top:5px;margin-bottom:5px;padding-left:5px;padding-right:5px;width:100%}";
		$content .= "#bbpress-forums ul.bbp-topics,#bbpress-forums ul.bbp-forums,#bbpress-forums ul.bbp-replies{border-left:2px solid $color_forum_canvas;border-right:2px solid $color_forum_canvas;border-radius:10px}";
		$content .= "#bbpress-forums .bbp-breadcrumb{background:#eee;border-left:solid 3px $color_link;margin-top:5px;margin-bottom:5px;padding-left:5px;padding-right:5px;width:100%}";
		$content .= "#bbpress-forums li.bbp-header{background:$color_forum_canvas;border-top-left-radius:5px;border-top-right-radius:5px;margin:0;padding:5px}";
		$content .= "#bbpress-forums li.bbp-footer{background:$color_forum_canvas;border-bottom-left-radius:5px;border-bottom-right-radius:5px;margin:0;padding:5px}";
		$content .= "#bbpress-forums div.odd,#bbpress-forums ul.odd{background-color:$color_forum_topic_background}";
		$content .= "#bbpress-forums div.even,#bbpress-forums ul.even{background-color:$color_forum_topic_background2}";
		$content .= "#bbpress-forums .hentry{margin:0px;padding:0}";
		$content .= "#bbpress-forums .hentry .bbp-reply-author{border-right:1px solid #fff}";
		$content .= "#bbpress-forums .bbp-pagination .bbp-pagination-count{text-align:right;display:block;width:100%}";
		$content .= "#bbpress-forums .bbp-pagination .bbp-pagination-links{float:right;display:block}#bbpress-forums .bbp-reply-form,#bbpress-forums .bbp-topic-form{background-color:$color_quote_background;border-radius:10px}";
		$content .= "#bbpress-forums .bbp-reply-form fieldset,#bbpress-forums .bbp-topic-form fieldset{border:none}";
		$content .= "#bbpress-forums .bbp-form legend{font-weight:bold;font-size:18px}";
		$content .= "#bbpress-forums .bbp-admin-links a,#bbpress-forums .button,.bbp_widget_login .logout-link,.bbp_widget_login .button{text-decoration:none;color:$color_link;text-transform:uppercase;padding-left:5px;padding-right:5px;display:inline-block;border:solid 1px #ccc;border-radius:5px;background:rgb(255,255,255);background:-moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(229,229,229,1) 100%);background:-webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(229,229,229,1)));background:-webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);background:-o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);background:-ms-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);background:linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0)}";
		$content .= "#bbpress-forums .bbp-admin-links a:hover,#bbpress-forums .button:hover,.bbp_widget_login .logout-link:hover,.bbp_widget_login .button:hover{opacity:0.8}";
		$content .= "#bbpress-forums .bbp-admin-links a:active,#bbpress-forums .button:active,.bbp_widget_login .logout-link:active,.bbp_widget_login .button:active{box-shadow:2px 2px 2px #ccc inset;-moz-box-shadow:2px 2px 2px #ccc inset;-webkit-box-shadow:2px 2px 2px #ccc inset}";
		$content .= "#bbpress-forums .button{padding:5px}";
		$content .= ".google-maps{position:relative;padding-bottom:75%;height:0;overflow:hidden}.google-maps iframe{position:absolute;top:0;left:0;width:100%!important;height:100%!important}";
		$content .= ".sprite{background-image:url(images/spritesheet.png);background-repeat:no-repeat;display:block}.sprite-blogger{width:32px;height:32px;background-position:-5px -5px}.sprite-delicious{width:32px;height:32px;background-position:-47px -5px}.sprite-designbumb{width:32px;height:32px;background-position:-89px -5px}.sprite-designfloat{width:32px;height:32px;background-position:-131px -5px}.sprite-designmoo{width:32px;height:32px;background-position:-173px -5px}.sprite-deviantart{width:32px;height:32px;background-position:-5px -47px}.sprite-digg{width:32px;height:32px;background-position:-47px -47px}.sprite-dribbble{width:32px;height:32px;background-position:-89px -47px}.sprite-drupal{width:32px;height:32px;background-position:-131px -47px}.sprite-facebook{width:32px;height:32px;background-position:-173px -47px}.sprite-forrst{width:32px;height:32px;background-position:-5px -89px}.sprite-foursquare{width:32px;height:32px;background-position:-47px -89px}.sprite-friendfeed{width:32px;height:32px;background-position:-89px -89px}.sprite-gmail{width:32px;height:32px;background-position:-131px -89px}.sprite-google{width:32px;height:32px;background-position:-173px -89px}.sprite-googleplus{width:32px;height:32px;background-position:-5px -131px}.sprite-html5{width:32px;height:32px;background-position:-47px -131px}.sprite-line{width:32px;height:32px;background-position:-89px -131px}.sprite-lastfm{width:32px;height:32px;background-position:-131px -131px}.sprite-linkedin{width:32px;height:32px;background-position:-173px -131px}.sprite-paypal{width:32px;height:32px;background-position:-5px -173px}.sprite-instagram{width:32px;height:32px;background-position:-47px -173px}.sprite-picasa2{width:32px;height:32px;background-position:-89px -173px}.sprite-pinterest{width:32px;height:32px;background-position:-131px -173px}.sprite-reddit{width:32px;height:32px;background-position:-173px -173px}.sprite-rss{width:32px;height:32px;background-position:-215px -5px}.sprite-skype{width:32px;height:32px;background-position:-215px -47px}.sprite-stumbleupon{width:32px;height:32px;background-position:-215px -89px}.sprite-technorati{width:32px;height:32px;background-position:-215px -131px}.sprite-tumblr{width:32px;height:32px;background-position:-215px -173px}.sprite-twitter{width:32px;height:32px;background-position:-5px -215px}.sprite-vimeo{width:32px;height:32px;background-position:-47px -215px}.sprite-wordpress{width:32px;height:32px;background-position:-89px -215px}.sprite-yahoo{width:32px;height:32px;background-position:-131px -215px}.sprite-youtube{width:32px;height:32px;background-position:-173px -215px}";
		$content .= ".flat-mega-menu a,.flat-mega-menu h2,.flat-mega-menu img,.flat-mega-menu li,.flat-mega-menu p,.flat-mega-menu ul{margin:0;padding:0;display:block;line-height:normal;text-decoration:none;list-style:none;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.flat-mega-menu{padding:0;position:relative;z-index:999;display:block;width:100%;margin:0 auto;max-width:$site_width}.flat-mega-menu>ul{float:left;width:100%;min-height:48px}.flat-mega-menu>ul>li{float:left;min-height:48px;padding-left:10px;padding-right:10px}.flat-mega-menu>ul>li>a{line-height:48px;width:100%;padding-left:10px;padding-right:10px;font-size:1em}.flat-mega-menu>ul>.title>a{font-size:1.4em;line-height:48px;padding-left:20px;text-align:left;padding-right:20px}.flat-mega-menu>ul>.title{border-left:none}.flat-mega-menu ul .login-form{text-align:center;cursor:pointer;float:right;padding:10px;position:relative}.flat-mega-menu .login-form table,.flat-mega-menu .login-form tbody{width:100%}.flat-mega-menu .login-form input{width:100%;margin:0 0 3px;padding:0 10px;display:block;border-radius:3px;background-color:#f4f4f4;box-shadow:inset .5px 1px 3px 0 rgba(0,0,0,.1);-webkit-box-shadow:inset .5px 1px 3px 0 rgba(0,0,0,.1);border:none;height:30px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;outline:0}.flat-mega-menu .login-form input[type=submit]{border:1px solid rgba(0,0,0,.15);border-radius:3px;opacity:.81;box-shadow:inset .5px .87px 0 0 rgba(255,248,68,.106);color:#FFF;height:35px;width:100px;font-size:1em;margin-top:10px;cursor:pointer;outline:0}.flat-mega-menu .login-form input[type=submit]:hover{opacity:1}.flat-mega-menu .login-form input[type=checkbox]{float:left;display:block;height:48px;width:auto;cursor:pointer;outline:0;background-color:#FFF}.flat-mega-menu .login-form label{color:#7D7D7D;font-size:.8em;font-weight:400;height:48px;display:block;margin:10px 0 0;padding:0;line-height:35px;white-space:nowrap;cursor:pointer;-webkit-user-select:none;-ms-user-select:none;-moz-user-select:none;-o-user-select:none}.flat-mega-menu .login-form ul{position:absolute;right:0;width:300px;padding:15px 15px 10px}.flat-mega-menu .login-form:hover{background-color:#FFF}.flat-mega-menu ul .search-bar{min-width:6%;text-align:center;cursor:pointer;float:right;padding:10px}.flat-mega-menu .search-bar:hover{opacity:.8}.flat-mega-menu .search-bar ul{position:absolute;right:0;width:50%}.flat-mega-menu .fa-user tbody,.flat-mega-menu .search-bar table{width:100%}.flat-mega-menu .search-bar table tr td:last-child{width:120px}.flat-mega-menu .search-bar ul input[type=text]{box-shadow:inset 0 0 2px 0 rgba(0,0,0,.55);-webkit-box-shadow:inset 0 0 2px 0 rgba(0,0,0,.55);border:none;padding:10px;width:100%;float:left;border-right:10px solid transparent;border-left:20px solid transparent;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;height:48px;outline:0}.flat-mega-menu .search-bar ul input[type=submit]{border:none;width:85%;cursor:pointer;font-size:.9em;float:left;height:40px;outline:0;border:1px solid rgba(0,0,0,.15);opacity:.81;box-shadow:inset .5px .87px 0 0 rgba(255,248,68,.106);color:#333}.flat-mega-menu .search-bar ul input[type=submit]:hover{opacity:1;outline:0}.flat-mega-menu>ul>.title:hover>a{background:inherit;color:inherit !important}.flat-mega-menu .drop-down{position:absolute;z-index:999;margin-left:0px;width:100%;float:left;width:150px;background-color:$color_menu_background}.flat-mega-menu .drop-down .drop-down{margin-top:10px;background-color:$color_menu_background}.flat-mega-menu .drop-down li{float:left;width:100%;position:relative}.flat-mega-menu .drop-down li a{font-size:.9em;text-align:left;padding:10px 20px 10px 25px}.flat-mega-menu ul li .drop-down li:hover>.drop-down,.flat-mega-menu>ul>li:hover .drop-down{display:block}.flat-mega-menu>ul>li .drop-down.hover-fade{display:none}.flat-mega-menu>ul>li .drop-down li:hover>.drop-down.hover-fade,.flat-mega-menu>ul>li:hover>.drop-down.hover-fade{display:block}.flat-mega-menu>ul>li .drop-down.hover-zoom{display:none}.flat-mega-menu>ul>li .drop-down li:hover>.drop-down.hover-zoom,.flat-mega-menu>ul>li:hover>.drop-down.hover-zoom{display:block}.flat-mega-menu>ul>li .drop-down.hover-expand{display:none}.flat-mega-menu>ul>li .drop-down li:hover>.drop-down.hover-expand,.flat-mega-menu>ul>li:hover>.drop-down.hover-expand{display:block}.flat-mega-menu .drop-down li i{position:absolute;z-index:999;top:0;right:20px;bottom:0;height:16px;margin:auto}.flat-mega-menu .drop-down .drop-down{left:100%;top:-10px;width:150px}.flat-mega-menu .drop-down.one-column{width:230px}.flat-mega-menu .drop-down .drop-down.one-column{width:200px}.flat-mega-menu .drop-down.two-column{width:322px}.flat-mega-menu .drop-down.two-column ul{float:left;width:160px;margin:0;padding:0}.flat-mega-menu .drop-down.full-width{width:100%;margin-left:0;left:0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.flat-mega-menu .drop-down.full-width ul{float:left;width:25%;cursor:default}.flat-mega-menu .drop-down.full-width ul li{margin:0;padding:10px 0;float:left;width:100%}.flat-mega-menu .drop-down.full-width ul li a{margin:0 10%;border:none;padding:0;float:left}.flat-mega-menu .drop-down.full-width ul li p{margin:0;padding:10px 10px 10px 11%;float:left;width:100%;clear:both;font-size:.9em;color:#949598}.flat-mega-menu .drop-down.social-bar{width:200px}.flat-mega-menu hr{margin:0;padding:0;display:block}.flat-mega-menu img{width:100%;float:left;border-style:solid;border-width:4px;border-color:#fff;background-color:#ebebeb;box-shadow:0 0 5px 0 rgba(0,0,0,.078),inset 0 0 76px 0 rgba(0,0,0,.039)}.flat-mega-menu img:hover{opacity:.8}.flat-mega-menu h2{color:#535456;font-size:1.2em;font-weight:400;margin:0;padding:15px 5px 10px 10%}.flat-mega-menu .social-bar{position:relative}.flat-mega-menu .social-bar ul{width:262px;padding:10px;position:absolute;right:0}.flat-mega-menu .social-bar ul li{float:left;width:40px;height:40px;margin:10px;padding:0}.flat-mega-menu .social-bar ul a{margin:0;padding:0;display:block;height:40px;width:40px;position:relative}.flat-mega-menu .social-bar ul a i{text-align:center;padding:0;margin:0;height:40px;width:40px;position:absolute;top:0;left:0;border:1px solid rgba(0,0,0,.1);line-height:40px;color:#797979;font-size:1.3em;border-radius:4px}.flat-mega-menu .social-bar ul a:hover i{background:#C7C7C7;color:#000}.flat-mega-menu>input,.flat-mega-menu>label{display:none}@media screen and (max-width:768px){.flat-mega-menu>ul>li{width:100%;border-left:none !important;border-right:none !important}.flat-mega-menu>ul>.title a{text-align:center}.flat-mega-menu .drop-down.full-width{left:0 !important}.flat-mega-menu .drop-down .drop-down,.flat-mega-menu .drop-down.full-width,.flat-mega-menu .drop-down.one-column,.flat-mega-menu .drop-down.two-column,.flat-mega-menu .login-form ul,.flat-mega-menu .search-bar ul,.flat-mega-menu .social-bar ul{width:100% !important;display:block !important;position:relative;left:0}.flat-mega-menu .drop-down .drop-down{border:none;top:0;padding:0}.flat-mega-menu .drop-down .drop-down.one-column{padding-left:20px}.flat-mega-menu>ul>li .drop-down.hover-expand,.flat-mega-menu>ul>li .drop-down.hover-fade,.flat-mega-menu>ul>li .drop-down.hover-zoom{display:none;border:none}.flat-mega-menu>ul>li .drop-down li:hover>.drop-down.hover-expand,.flat-mega-menu>ul>li .drop-down li:hover>.drop-down.hover-fade,.flat-mega-menu>ul>li .drop-down li:hover>.drop-down.hover-zoom,.flat-mega-menu>ul>li:hover>.drop-down.hover-expand,.flat-mega-menu>ul>li:hover>.drop-down.hover-fade,.flat-mega-menu>ul>li:hover>.drop-down.hover-zoom{display:block}.flat-mega-menu>ul>.login-form:hover,.flat-mega-menu>ul>.search-bar:hover,.flat-mega-menu>ul>li:hover>a{background:inherit}.flat-mega-menu ul .login-form,.flat-mega-menu ul .search-bar{margin:0;padding:0}.flat-mega-menu ul .login-form label{text-align:left;text-indent:10px}.flat-mega-menu ul .login-form table td{cursor:default}.flat-mega-menu .drop-down.full-width ul,.flat-mega-menu .drop-down.two-column ul{width:50%}.flat-mega-menu .drop-down.one-column a,.flat-mega-menu .drop-down.two-column a{width:auto;float:left}.flat-mega-menu ul .social-bar{text-align:center}.flat-mega-menu ul .social-bar ul li{display:inline-block;float:none}.flat-mega-menu .drop-down li .fa.fa-angle-right{position:relative;z-index:999;right:0;float:right;height:auto;color:#555658;padding:0;margin:0 17px 0 0;display:block;line-height:38px}.flat-mega-menu #mobile-button{position:absolute;display:none}.flat-mega-menu>ul{height:48px;overflow:hidden}.flat-mega-menu #mobile-button:checked+ul{height:auto;overflow:visible}.flat-mega-menu>label{width:48px;height:48px;display:block;margin:0;padding:0;position:absolute;top:0;right:0;text-align:center;line-height:48px;font-size:2em;color:#ccc;cursor:pointer}.flat-mega-menu>label i{-ms-user-select:none;-moz-user-select:none;-o-user-select:none;-webkit-user-select:none}.flat-mega-menu ul .title{padding-left:60px}.flat-mega-menu .collapse .drop-down .drop-down,.flat-mega-menu .collapse .drop-down.full-width,.flat-mega-menu .collapse .drop-down.one-column,.flat-mega-menu .collapse .drop-down.two-column,.flat-mega-menu .collapse .login-form ul,.flat-mega-menu .collapse .search-bar ul,.flat-mega-menu .collapse .social-bar ul{width:100% !important;display:none !important;position:relative;left:0}.flat-mega-menu>.collapse>li .drop-down li:hover>.drop-down.hover-expand,.flat-mega-menu>.collapse>li .drop-down li:hover>.drop-down.hover-fade,.flat-mega-menu>.collapse>li .drop-down li:hover>.drop-down.hover-zoom,.flat-mega-menu>.collapse>li:hover>.drop-down.hover-expand,.flat-mega-menu>.collapse>li:hover>.drop-down.hover-fade,.flat-mega-menu>.collapse>li:hover>.drop-down.hover-zoom{display:block !important}.primary{width:auto}.full-page{width:auto}}@media screen and (max-width:320px){.primary{width:auto}.full-page{width:auto}.flat-mega-menu .drop-down.full-width ul,.flat-mega-menu .drop-down.two-column ul{width:100%}.flat-mega-menu ul .search-bar td{width:100% !important;float:left;padding:5px 20px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;cursor:default}.flat-mega-menu ul .search-bar input[type=submit],.flat-mega-menu ul .search-bar input[type=text]{width:100%;margin:0;border:none}.flat-mega-menu ul .login-form ul{padding:20px}.flat-mega-menu ul .login-form td{float:left;width:100%;cursor:default}.flat-mega-menu ul .login-form input[type=submit]{width:100%}.flat-mega-menu ul .title{padding-left:0;text-indent:60px}}";

		fwrite($file, $content);

		$options = null;
	}
}