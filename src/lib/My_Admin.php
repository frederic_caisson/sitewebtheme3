<?php
include ('My_Features.php');
include ('My_Posts.php');
include ('My_Theme_Settings.php');
include ('My_Css.php');
include ('My_Plugin_Contact.php');

class My_Admin {
    private $admin_features;
    private $admin_posts;
    private $admin_categories;
    private $admin_settings;
    private $plugin_contact;

    public function __construct(){
        $this->load_theme_scripts();
        $this->customize_login();
        $this->plugin_contact = new My_Plugin_Contact();
        if(is_admin()){
            $css = new My_Css();
            $this->admin_features = new My_Features();
            $this->admin_posts = new My_Posts();
            $this->admin_settings = new My_Theme_Settings($css);
        }
        
    }

    public function __destruct(){
        $this->admin_features = null;
        $this->admin_posts = null;
        $this->admin_categories = null;
        $this->admin_settings = null;
        $this->plugin_contact = null;
    }

    private function customize_login(){
        add_action('login_head', array( $this , 'customize_login_hook' ));
    }

    private function load_theme_scripts(){
        add_action( 'admin_enqueue_scripts', array( $this , 'prepare_scripts_hook' ));
    }

    public function prepare_scripts_hook(){
        wp_enqueue_script( 'token-input', get_template_directory_uri() . '/js/admin.js');
        wp_enqueue_style( 'token-input', get_template_directory_uri().'/css/token-input.css' );
    }

    public function customize_login_hook(){

        $logo_image = get_header_image();
        if($logo_image){
echo <<<EOF
<style type="text/css">
h1 a{
background:none;
}
.login h1 a {
background-image:url({$logo_image}) !important; 
}
</style>
EOF;
        }
    }
    
}