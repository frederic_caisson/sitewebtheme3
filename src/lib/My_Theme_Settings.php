<?php
class My_Theme_Settings
{
    const SLUG = 'my-theme-settings';
    const OPTION_GROUP = 'my_theme_options_group';
    private $options;
    private $css;
    private $defaults;

    public function __construct($css){
    	$this->css = $css;
        $this->defaults = $css->get_defaults();
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    public function __destruct(){
    	$this->css = null;
    	$this->defaults = null;
        $this->options = null;
    }

    /**
     * Add options page
     */
    public function add_plugin_page(){
        // This page will be under "Settings"
        add_menu_page(
            __('Theme Settings',THEME_NAME), 
            __('Theme Settings',THEME_NAME), 
            'manage_options', 
            self::SLUG, 
            array($this,'create_settings_page'),
            'dashicons-art'
        );
    }

    public function generate_css(){
    	$this->css->create();
    }

    /**
     * Options page callback
     */
    public function create_settings_page(){
        // Set class property
        $options = get_option(MY_OPTION_NAME);

        if(!empty($options['reset'])){
            delete_option(MY_OPTION_NAME);
            $this->css->set_options($this->defaults);
            $this->generate_css();
        } else {
            if(!empty($options['status'])){
                $this->options = $options;
                $this->options['status'] = 0;
                update_option(MY_OPTION_NAME,$this->options);
                $this->css->set_options($this->options);
                $this->generate_css();
            } else {
                $this->options = $options;
            }
        }

        ?>
        <div class="wrap">
            <h2><?php echo __('Theme Settings',THEME_NAME); ?></h2>           
            <form id="form_settings" method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( MY_OPTION_NAME_GROUP );   
                do_settings_sections( self::SLUG );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init(){

        register_setting(
            MY_OPTION_NAME_GROUP, // Option group
            MY_OPTION_NAME, // Option name
            array($this, 'sanitize')
        );

        $setting_section_id = 'my_theme_options_section1';
        add_settings_section(
            $setting_section_id, // ID
            __('General settings', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

        add_settings_field(
            'site_width', // ID
            __('Site max width in px', THEME_NAME), // Title 
            array( $this, 'input_site_width_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'site_width', // ID
            __('Site max width in px', THEME_NAME), // Title 
            array( $this, 'input_site_width_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'hide_search', // ID
            __('Hide search input', THEME_NAME), // Title 
            array( $this, 'checkbox_hide_search_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        
        add_settings_field(
            'site_width', // ID
            __('Site max width in px', THEME_NAME), // Title 
            array( $this, 'input_site_width_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'site_layout', // ID
            __('Site layout site', THEME_NAME), // Title 
            array( $this, 'radio_site_layout_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'sidebar_width', // ID
            __('Sidebars width in px', THEME_NAME), // Title 
            array( $this, 'input_sidebar_width_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'articles_organization', // ID
            __('Articles organization', THEME_NAME), // Title 
            array( $this, 'radio_articles_organization_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        $setting_section_id = 'my_theme_options_section02';
        add_settings_section(
            $setting_section_id, // ID
            __('Articles and pages settings', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

        add_settings_field(
            'hide_comments', // ID
            __('Hide comments', THEME_NAME), // Title 
            array( $this, 'checkbox_hide_comments_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'hide_author', // ID
            __('Hide author', THEME_NAME), // Title 
            array( $this, 'checkbox_hide_author_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'show_related_post', // ID
            __('Show related posts', THEME_NAME), // Title 
            array( $this, 'checkbox_show_related_posts_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );



        $setting_section_id = 'my_theme_options_section2';
        add_settings_section(
            $setting_section_id, // ID
            __('Featured categories', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

        add_settings_field(
            'categories_homepage', // ID
            __('Categories on homepage', THEME_NAME), // Title 
            array( $this, 'input_categories_homepage_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        $setting_section_id = 'my_theme_options_section3';
        add_settings_section(
            $setting_section_id, // ID
            __('Marketing settings', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

        add_settings_field(
            'feature_home', // ID
            __('Feature on homepage', THEME_NAME), // Title 
            array( $this, 'select_feature_home_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        $setting_section_id = 'my_theme_options_section4';

        add_settings_section(
            $setting_section_id, // ID
            __('Advertisement settings', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

        add_settings_field(
            'top_ad', // ID
            __('Top Ad', THEME_NAME), // Title 
            array( $this, 'textarea_top_ad_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'list_ad', // ID
            __('Random Ad list', THEME_NAME), // Title 
            array( $this, 'textarea_list_ad_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'bottom_ad', // ID
            __('Bottom ad', THEME_NAME), // Title 
            array( $this, 'textarea_bottom_ad_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );


        $setting_section_id = 'my_theme_options_section5';

        add_settings_section(
            $setting_section_id, // ID
            __('Color settings', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

        add_settings_field(
            'color_site_title', // ID
            __('Site title color', THEME_NAME), // Title 
            array( $this, 'input_color_site_title_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_subtitle', // ID
            __('Subtitle color', THEME_NAME), // Title 
            array( $this, 'input_color_subtitle_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_header_background', // ID
            __('Header background color', THEME_NAME), // Title 
            array( $this, 'input_color_header_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_link', // ID
            __('Link color', THEME_NAME), // Title 
            array( $this, 'input_color_link_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_link_hover', // ID
            __('Link color hover', THEME_NAME), // Title 
            array( $this, 'input_color_link_hover_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_menu_background', // ID
            __('Menu background color', THEME_NAME), // Title 
            array( $this, 'input_color_menu_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_content_background', // ID
            __('Content background color', THEME_NAME), // Title 
            array( $this, 'input_color_content_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_category_title_background', // ID
            __('Featured category title background color', THEME_NAME), // Title 
            array( $this, 'input_color_category_title_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_category_title_font', // ID
            __('Featured category title font color', THEME_NAME), // Title 
            array( $this, 'input_color_category_title_font_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_sidebar_background', // ID
            __('Sidebar background color', THEME_NAME), // Title 
            array( $this, 'input_color_sidebar_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_sidebar_title_background', // ID
            __('Sidebar title background color', THEME_NAME), // Title 
            array( $this, 'input_color_sidebar_title_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_sidebar_title_font', // ID
            __('Sidebar title font color', THEME_NAME), // Title 
            array( $this, 'input_color_sidebar_title_font_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_footer_background', // ID
            __('Footer background color', THEME_NAME), // Title 
            array( $this, 'input_color_footer_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_footer_font', // ID
            __('Footer font color', THEME_NAME), // Title 
            array( $this, 'input_color_footer_font_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_primary_button_background', // ID
            __('Primary button background color', THEME_NAME), // Title 
            array( $this, 'input_color_primary_button_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_primary_button_background_shadow', // ID
            __('Primary button background shadow color', THEME_NAME), // Title 
            array( $this, 'input_color_primary_button_background_shadow_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_primary_button_font', // ID
            __('Primary button font color', THEME_NAME), // Title 
            array( $this, 'input_color_primary_button_font_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_action_button_background', // ID
            __('Action button background color', THEME_NAME), // Title 
            array( $this, 'input_color_action_button_background_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_action_button_background_shadow', // ID
            __('Action button background shadow color', THEME_NAME), // Title 
            array( $this, 'input_color_action_button_background_shadow_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_action_button_font', // ID
            __('Action button font color', THEME_NAME), // Title 
            array( $this, 'input_color_action_button_font_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'color_forum_canvas', // ID
            __('Forum canvas color (if use bbpress)', THEME_NAME), // Title 
            array( $this, 'input_color_forum_canvas_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        $setting_section_id = 'my_theme_options_section6';

        add_settings_section(
            $setting_section_id, // ID
            __('Footer settings', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

         add_settings_field(
            'year', // ID
            __('Year (of start)', THEME_NAME), // Title 
            array( $this, 'input_year_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'copyright', // ID
            __('Copyright', THEME_NAME), // Title 
            array( $this, 'textarea_copyright_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        $setting_section_id = 'my_theme_options_section7';
        add_settings_section(
            $setting_section_id, // ID
            __('Social network settings', THEME_NAME), // Title
            array( $this, 'print_section_info' ), // Callback
            self::SLUG // Page
        );

        add_settings_field(
            'facebook_url', // ID
            __('Facebook url', THEME_NAME), // Title 
            array( $this, 'input_facebook_url_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'twitter_url', // ID
            __('Twitter url', THEME_NAME), // Title 
            array( $this, 'input_twitter_url_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'googleplus_url', // ID
            __('Google+ url', THEME_NAME), // Title 
            array( $this, 'input_googleplus_url_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'youtube_url', // ID
            __('Youtube url', THEME_NAME), // Title 
            array( $this, 'input_youtube_url_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'instagram_url', // ID
            __('Instagram url', THEME_NAME), // Title 
            array( $this, 'input_instagram_url_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'pinterest_url', // ID
            __('Pinterest url', THEME_NAME), // Title 
            array( $this, 'input_pinterest_url_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'my_theme_options', // ID
            '', // Title 
            array( $this, 'input_my_theme_options_status_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );

        add_settings_field(
            'reset_button', // ID
             __('Reset settings', THEME_NAME), // Title 
            array( $this, 'input_reset_button_callback' ), // Callback
            self::SLUG, // Page
            $setting_section_id // Section           
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input){
        $new_input = array();
        
        $allowed = array(
            'script'    => array(
                'src' => array()
            )
        );

        if( isset( $input['site_layout'] ) )
            $new_input['site_layout'] = wp_kses( $input['site_layout'], $allowed );

        if( isset( $input['site_width'] ) )
            $new_input['site_width'] = wp_kses( $input['site_width'], $allowed );

        if( isset( $input['sidebar_left_width'] ) )
            $new_input['sidebar_left_width'] = wp_kses( $input['sidebar_left_width'], $allowed );

        if( isset( $input['sidebar_right_width'] ) )
            $new_input['sidebar_right_width'] = wp_kses( $input['sidebar_right_width'], $allowed );

        if( isset( $input['categories_homepage'] ) )
            $new_input['categories_homepage'] = wp_kses( $input['categories_homepage'], $allowed );

        if( isset( $input['articles_organization'] ) )
            $new_input['articles_organization'] = wp_kses( $input['articles_organization'], $allowed );

        if( isset( $input['hide_search'] ) )
            $new_input['hide_search'] = wp_kses( $input['hide_search'], $allowed );

        if( isset( $input['hide_comments_on_post'] ) )
            $new_input['hide_comments_on_post'] = wp_kses( $input['hide_comments_on_post'], $allowed );

        if( isset( $input['hide_comments_on_page'] ) )
            $new_input['hide_comments_on_page'] = wp_kses( $input['hide_comments_on_page'], $allowed );

        if( isset( $input['hide_author_on_post'] ) )
            $new_input['hide_author_on_post'] = wp_kses( $input['hide_author_on_post'], $allowed );

        if( isset( $input['hide_author_on_page'] ) )
            $new_input['hide_author_on_page'] = wp_kses( $input['hide_author_on_page'], $allowed );

        if( isset( $input['show_related_posts_on_post'] ) )
            $new_input['show_related_posts_on_post'] = wp_kses( $input['show_related_posts_on_post'], $allowed );

        if( isset( $input['feature_home'] ) )
            $new_input['feature_home'] = wp_kses( $input['feature_home'], $allowed );

        if( isset( $input['top_ad'] ) )
            $new_input['top_ad'] = ($input['top_ad']);

        if( isset( $input['bottom_ad'] ) )
            $new_input['bottom_ad'] = ($input['bottom_ad']);

        if( isset( $input['list_ad'] ) )
            $new_input['list_ad'] = ($input['list_ad']);

        if( isset( $input['copyright'] ) )
            $new_input['copyright'] = ($input['copyright']);

        if( isset( $input['color_site_title'] ) )
            $new_input['color_site_title'] = wp_kses( $input['color_site_title'], $allowed );

        if( isset( $input['color_subtitle'] ) )
            $new_input['color_subtitle'] = wp_kses( $input['color_subtitle'], $allowed );

        if( isset( $input['color_header_background'] ) )
            $new_input['color_header_background'] = wp_kses( $input['color_header_background'], $allowed );

        if( isset( $input['color_link'] ) )
            $new_input['color_link'] = wp_kses( $input['color_link'], $allowed );

        if( isset( $input['color_link_hover'] ) )
            $new_input['color_link_hover'] = wp_kses( $input['color_link_hover'], $allowed );

        if( isset( $input['color_menu_background'] ) )
            $new_input['color_menu_background'] = wp_kses( $input['color_menu_background'], $allowed );

        if( isset( $input['color_content_background'] ) )
            $new_input['color_content_background'] = wp_kses( $input['color_content_background'], $allowed );

        if( isset( $input['color_sidebar_background'] ) )
            $new_input['color_sidebar_background'] = wp_kses( $input['color_sidebar_background'], $allowed );

        if( isset( $input['color_sidebar_title_background'] ) )
            $new_input['color_sidebar_title_background'] = wp_kses( $input['color_sidebar_title_background'], $allowed );

        if( isset( $input['color_sidebar_title_font'] ) )
            $new_input['color_sidebar_title_font'] = wp_kses( $input['color_sidebar_title_font'], $allowed );

        if( isset( $input['color_footer_background'] ) )
            $new_input['color_footer_background'] = wp_kses( $input['color_footer_background'], $allowed );

        if( isset( $input['color_footer_font'] ) )
            $new_input['color_footer_font'] = wp_kses( $input['color_footer_font'], $allowed );

         if( isset( $input['color_primary_button_background'] ) )
            $new_input['color_primary_button_background'] = wp_kses( $input['color_primary_button_background'], $allowed );

        if( isset( $input['color_primary_button_background_shadow'] ) )
            $new_input['color_primary_button_background_shadow'] = wp_kses( $input['color_primary_button_background_shadow'], $allowed );

        if( isset( $input['color_primary_button_font'] ) )
            $new_input['color_primary_button_font'] = wp_kses( $input['color_primary_button_font'], $allowed );

        if( isset( $input['color_action_button_background'] ) )
            $new_input['color_action_button_background'] = wp_kses( $input['color_action_button_background'], $allowed );
        
        if( isset( $input['color_action_button_background_shadow'] ) )
            $new_input['color_action_button_background_shadow'] = wp_kses( $input['color_action_button_background_shadow'], $allowed );

        if( isset( $input['color_action_button_font'] ) )
            $new_input['color_action_button_font'] = wp_kses( $input['color_action_button_font'], $allowed );

        if( isset( $input['color_forum_canvas'] ) )
            $new_input['color_forum_canvas'] = wp_kses( $input['color_forum_canvas'], $allowed );

        if( isset( $input['color_category_title_background'] ) )
            $new_input['color_category_title_background'] = wp_kses( $input['color_category_title_background'], $allowed );

        if( isset( $input['color_category_title_font'] ) )
            $new_input['color_category_title_font'] = wp_kses( $input['color_category_title_font'], $allowed );

        if( isset( $input['year'] ) )
            $new_input['year'] = wp_kses( $input['year'], $allowed );

        if( isset( $input['year'] ) )
            $new_input['year'] = wp_kses( $input['year'], $allowed );

        if( isset( $input['year'] ) )
            $new_input['year'] = wp_kses( $input['year'], $allowed );

        if( isset( $input['facebook_url'] ) )
            $new_input['facebook_url'] = wp_kses( $input['facebook_url'], $allowed );

        if( isset( $input['twitter_url'] ) )
            $new_input['twitter_url'] = wp_kses( $input['twitter_url'], $allowed );

        if( isset( $input['googleplus_url'] ) )
            $new_input['googleplus_url'] = wp_kses( $input['googleplus_url'], $allowed );

        if( isset( $input['youtube_url'] ) )
            $new_input['youtube_url'] = wp_kses( $input['youtube_url'], $allowed );

        if( isset( $input['instagram_url'] ) )
            $new_input['instagram_url'] = wp_kses( $input['instagram_url'], $allowed );

        if( isset( $input['pinterest_url'] ) )
            $new_input['pinterest_url'] = wp_kses( $input['pinterest_url'], $allowed );

        if( isset( $input['status'] ) )
            $new_input['status'] = wp_kses( $input['status'], $allowed );

        if( isset( $input['reset'] ) )
            $new_input['reset'] = wp_kses( $input['reset'], $allowed );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info(){
        echo '<hr>';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function input_reset_button_callback(){

    	$value = __('Reset', THEME_NAME);
        echo '<input type="submit" name="'.MY_OPTION_NAME.'[reset]" class="button button-secondary" value="'.$value.'">';
    }

    public function input_my_theme_options_status_callback(){
       
        echo '<input type="hidden" id="status" name="'.MY_OPTION_NAME.'[status]" value="1" class="regular-text" >';
    }

    public function textarea_top_ad_callback(){
        printf(
            '<textarea class="group-ads" rows="4" cols="50" name="'.MY_OPTION_NAME.'[top_ad]">%s</textarea>',
            isset( $this->options['top_ad'] ) ? $this->options['top_ad'] : $this->defaults['top_ad']
        );
    }

    public function textarea_bottom_ad_callback(){
        printf(
            '<textarea class="group-ads" rows="4" cols="50" name="'.MY_OPTION_NAME.'[bottom_ad]">%s</textarea>',
            isset( $this->options['bottom_ad'] ) ? $this->options['bottom_ad'] : $this->defaults['bottom_ad']
        );
    }

    public function textarea_list_ad_callback(){
        printf(
            '<textarea class="group-ads" rows="4" cols="50" name="'.MY_OPTION_NAME.'[list_ad]">%s</textarea>',
            isset( $this->options['list_ad'] ) ? $this->options['list_ad'] : $this->defaults['list_ad']
        );
    }

    public function input_facebook_url_callback(){
        printf(
            '<input type="url" name="'.MY_OPTION_NAME.'[facebook_url]" value="%s" class="regular-text" >',
            isset( $this->options['facebook_url'] ) ? esc_attr( $this->options['facebook_url']) : $this->defaults['facebook_url']
        );
    }

    public function input_twitter_url_callback(){
        printf(
            '<input type="url" name="'.MY_OPTION_NAME.'[twitter_url]" value="%s" class="regular-text" >',
            isset( $this->options['twitter_url'] ) ? esc_attr( $this->options['twitter_url']) : $this->defaults['twitter_url']
        );
    }

    public function input_googleplus_url_callback(){
        printf(
            '<input type="url" name="'.MY_OPTION_NAME.'[googleplus_url]" value="%s" class="regular-text" >',
            isset( $this->options['googleplus_url'] ) ? esc_attr( $this->options['googleplus_url']) : $this->defaults['googleplus_url']
        );
    }

    public function input_youtube_url_callback(){
        printf(
            '<input type="url" name="'.MY_OPTION_NAME.'[youtube_url]" value="%s" class="regular-text" >',
            isset( $this->options['youtube_url'] ) ? esc_attr( $this->options['youtube_url']) : $this->defaults['youtube_url']
        );
    }

    public function input_instagram_url_callback(){
        printf(
            '<input type="url" name="'.MY_OPTION_NAME.'[instagram_url]" value="%s" class="regular-text" >',
            isset( $this->options['instagram_url'] ) ? esc_attr( $this->options['instagram_url']) : $this->defaults['instagram_url']
        );
    }

    public function input_pinterest_url_callback(){
        printf(
            '<input type="url" name="'.MY_OPTION_NAME.'[pinterest_url]" value="%s" class="regular-text" >',
            isset( $this->options['pinterest_url'] ) ? esc_attr( $this->options['pinterest_url']) : $this->defaults['pinterest_url']
        );
    }

    public function checkbox_hide_search_callback(){
        $html = '<input type="checkbox" name="'.MY_OPTION_NAME.'[hide_search]" value="1"' . checked( 1, $this->options['hide_search'], false ) . '>';
        echo $html;
    }

    public function checkbox_hide_comments_callback(){
        $html = '<input type="checkbox" name="'.MY_OPTION_NAME.'[hide_comments_on_post]" value="1"' . checked( 1, $this->options['hide_comments_on_post'], false ) . '>';
        echo __('On articles', THEME_NAME).' '.$html;

        $html = '<input type="checkbox" name="'.MY_OPTION_NAME.'[hide_comments_on_page]" value="1"' . checked( 1, $this->options['hide_comments_on_page'], false ) . '>';
        echo __('On pages', THEME_NAME).' '.$html;
    }

    public function checkbox_hide_author_callback(){
        $html = '<input type="checkbox" name="'.MY_OPTION_NAME.'[hide_author_on_post]" value="1"' . checked( 1, $this->options['hide_author_on_post'], false ) . '>';
        echo __('On articles', THEME_NAME).' '.$html;

        $html = '<input type="checkbox" name="'.MY_OPTION_NAME.'[hide_author_on_page]" value="1"' . checked( 1, $this->options['hide_author_on_page'], false ) . '>';
        echo __('On pages', THEME_NAME).' '.$html;
    }

    public function checkbox_show_related_posts_callback(){
        $html = '<input type="checkbox" name="'.MY_OPTION_NAME.'[show_related_posts_on_post]" value="1"' . checked( 1, $this->options['show_related_posts_on_post'], false ) . '>';
        echo __('On articles', THEME_NAME).' '.$html;

        /*$html = '<input type="checkbox" name="'.MY_OPTION_NAME.'[show_related_posts_on_page]" value="1"' . checked( 1, $this->options['show_related_posts_on_page'], false ) . '>';
        echo __('On pages', THEME_NAME).' '.$html;*/
    }

    public function input_site_width_callback(){
        printf(
            '<input type="number" name="'.MY_OPTION_NAME.'[site_width]" value="%s" min="400" max="1920" maxlength="4" >',
            isset( $this->options['site_width'] ) ? esc_attr( $this->options['site_width']) : $this->defaults['site_width']
        );
    }

    public function input_sidebar_width_callback(){

        printf(
            '<label>'.__('Left sidebar',THEME_NAME).'</label> <input type="number" name="'.MY_OPTION_NAME.'[sidebar_left_width]" value="%s" min="50" max="800" maxlength="3"> ',
            isset( $this->options['sidebar_left_width'] ) ? esc_attr( $this->options['sidebar_left_width']) : $this->defaults['sidebar_left_width']
        );

        printf(
            '<label>'.__('Right sidebar',THEME_NAME).'</label> <input type="number" name="'.MY_OPTION_NAME.'[sidebar_right_width]" value="%s" min="50" max="800" maxlength="3"> ',
            isset( $this->options['sidebar_right_width'] ) ? esc_attr( $this->options['sidebar_right_width']) : $this->defaults['sidebar_right_width']
        );
    }

    public function radio_site_layout_callback(){
        $radio = $this->options['site_layout'];
        if(empty($radio)){
            $radio = $this->defaults['site_layout'];
        }
    	$html = '<input type="radio" name="'.MY_OPTION_NAME.'[site_layout]" value="1"' . checked( 1, $radio, false ) . '>';
	    $html .= '<label for="site_layout_radio1">'.__('1 column', THEME_NAME).'</label>';
	    $html .= '&nbsp;';
	    $html .= '<input type="radio" name="'.MY_OPTION_NAME.'[site_layout]" value="2"' . checked( 2, $radio, false ) . '>';
	    $html .= '<label for="site_layout_radio2">'.__('2 columns (sidebar left)', THEME_NAME).'</label>';
	    $html .= '&nbsp;';
	    $html .= '<input type="radio" name="'.MY_OPTION_NAME.'[site_layout]" value="3"' . checked( 3, $radio, false ) . '>';
	    $html .= '<label for="site_layout_radio3">'.__('2 columns (sidebar right)', THEME_NAME).'</label>';
        $html .= '&nbsp;';
        $html .= '<input type="radio" name="'.MY_OPTION_NAME.'[site_layout]" value="4"' . checked( 4, $radio, false ) . '>';
        $html .= '<label for="site_layout_radio4">'.__('3 columns', THEME_NAME).'</label>';
	    echo $html;
    }

    public function radio_articles_organization_callback(){
        $radio = $this->options['articles_organization'];
        if(empty($radio)){
            $radio = $this->defaults['articles_organization'];
        }
        $html = '<input type="radio" name="'.MY_OPTION_NAME.'[articles_organization]" value="1"' . checked( 1, $radio, false ) . '>';
        $html .= '<label for="articles_organization_radio1">'.__('Blog articles', THEME_NAME).'</label>';
        $html .= '&nbsp;';
        $html .= '<input type="radio" name="'.MY_OPTION_NAME.'[articles_organization]" value="2"' . checked( 2, $radio, false ) . '>';
        $html .= '<label for="articles_organization_radio2">'.__('Featured categories', THEME_NAME).'</label>';
        $html .= '&nbsp;';
        $html .= '<input type="radio" name="'.MY_OPTION_NAME.'[articles_organization]" value="3"' . checked( 3, $radio, false ) . '>';
        $html .= '<label for="articles_organization_radio3">'.__('Ads optimized', THEME_NAME).'</label>';
        echo $html;
    }

    public function textarea_copyright_callback(){
        printf(
            '<textarea rows="4" cols="50" name="'.MY_OPTION_NAME.'[copyright]">%s</textarea>',
            isset( $this->options['copyright'] ) ? esc_attr($this->options['copyright']) : $this->defaults['copyright']
        );
    }

    public function input_year_callback(){
        printf(
            '<input type="number" name="'.MY_OPTION_NAME.'[year]" value="%s" min="1800" max="4000" maxlength="4" >',
            isset( $this->options['year'] ) ? esc_attr( $this->options['year']) : $this->defaults['year']
        );
    }

    public function input_color_site_title_callback(){

        if(isset( $this->options['color_site_title'])){
            $value = esc_attr($this->options['color_site_title']);
        } else {
            $value = $this->defaults['color_site_title'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_site_title]" value="%s" >', $value
        );
    }

    public function input_color_subtitle_callback(){

        if(isset( $this->options['color_subtitle'])){
            $value = esc_attr($this->options['color_subtitle']);
        } else {
            $value = $this->defaults['color_subtitle'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_subtitle]" value="%s" >', $value
        );
    }

    public function input_color_header_background_callback(){

        if(isset( $this->options['color_header_background'])){
            $value = esc_attr($this->options['color_header_background']);
        } else {
            $value = $this->defaults['color_header_background'];
        }
        printf(
            '<input class="jscolor {hash:true,required:false}" name="'.MY_OPTION_NAME.'[color_header_background]" value="%s" >', $value
        );
    }

    public function input_color_link_callback(){

        if(isset( $this->options['color_link'])){
            $value = esc_attr($this->options['color_link']);
        } else {
            $value = $this->defaults['color_link'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_link]" value="%s" >', $value
        );
    }

    public function input_color_link_hover_callback(){

        if(isset( $this->options['color_link_hover'])){
            $value = esc_attr($this->options['color_link_hover']);
        } else {
            $value = $this->defaults['color_link_hover'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_link_hover]" value="%s" >', $value
        );
    }

    public function input_color_menu_background_callback(){

        if(isset( $this->options['color_menu_background'])){
            $value = esc_attr($this->options['color_menu_background']);
        } else {
            $value = $this->defaults['color_menu_background'];
        }
        printf(
            '<input class="jscolor {hash:true,required:false}" name="'.MY_OPTION_NAME.'[color_menu_background]" value="%s" >', $value
        );
    }

    public function input_color_category_title_background_callback(){

        if(isset( $this->options['color_category_title_background'])){
            $value = esc_attr($this->options['color_category_title_background']);
        } else {
            $value = $this->defaults['color_category_title_background'];
        }
        printf(
            '<input class="jscolor {hash:true,required:false}" name="'.MY_OPTION_NAME.'[color_category_title_background]" value="%s" >', $value
        );
    }

    public function input_color_category_title_font_callback(){

        if(isset( $this->options['color_category_title_font'])){
            $value = esc_attr($this->options['color_category_title_font']);
        } else {
            $value = $this->defaults['color_category_title_font'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_category_title_font]" value="%s" >', $value
        );
    }

    public function input_color_content_background_callback(){

        if(isset( $this->options['color_content_background'])){
            $value = esc_attr($this->options['color_content_background']);
        } else {
            $value = $this->defaults['color_content_background'];
        }
        printf(
            '<input class="jscolor {hash:true,required:false}" name="'.MY_OPTION_NAME.'[color_content_background]" value="%s" >', $value
        );
    }

    public function input_color_sidebar_background_callback(){

        if(isset( $this->options['color_sidebar_background'])){
            $value = esc_attr($this->options['color_sidebar_background']);
        } else {
            $value = $this->defaults['color_sidebar_background'];
        }
        printf(
            '<input class="jscolor {hash:true,required:false}" name="'.MY_OPTION_NAME.'[color_sidebar_background]" value="%s" >', $value
        );
    }

    public function input_color_sidebar_title_background_callback(){
        if(isset( $this->options['color_sidebar_title_background'])){
            $value = esc_attr($this->options['color_sidebar_title_background']);
        } else {
            $value = $this->defaults['color_sidebar_title_background'];
        }
        printf(
            '<input class="jscolor {hash:true,required:false}" name="'.MY_OPTION_NAME.'[color_sidebar_title_background]" value="%s" >', $value
        );
    }

    public function input_color_sidebar_title_font_callback(){

        if(isset( $this->options['color_sidebar_title_font'])){
            $value = esc_attr($this->options['color_sidebar_title_font']);
        } else {
            $value = $this->defaults['color_sidebar_title_font'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_sidebar_title_font]" value="%s" >', $value
        );
    }

    public function input_color_footer_background_callback(){

        if(isset( $this->options['color_footer_background'])){
            $value = esc_attr($this->options['color_footer_background']);
        } else {
            $value = $this->defaults['color_footer_background'];
        }
        printf(
            '<input class="jscolor {hash:true,required:false}" name="'.MY_OPTION_NAME.'[color_footer_background]" value="%s" >', $value
        );
    }

    public function input_color_footer_font_callback(){

        if(isset( $this->options['color_footer_font'])){
            $value = esc_attr($this->options['color_footer_font']);
        } else {
            $value = $this->defaults['color_footer_font'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_footer_font]" value="%s" >', $value
        );
    }

    public function input_color_primary_button_background_callback(){

        if(isset( $this->options['color_primary_button_background'])){
            $value = esc_attr($this->options['color_primary_button_background']);
        } else {
            $value = $this->defaults['color_primary_button_background'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_primary_button_background]" value="%s" >', $value
        );
    }

    public function input_color_primary_button_background_shadow_callback(){

        if(isset( $this->options['color_primary_button_background_shadow'])){
            $value = esc_attr($this->options['color_primary_button_background_shadow']);
        } else {
            $value = $this->defaults['color_primary_button_background_shadow'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_primary_button_background_shadow]" value="%s" >', $value
        );
    }

    public function input_color_primary_button_font_callback(){

        if(isset( $this->options['color_primary_button_font'])){
            $value = esc_attr($this->options['color_primary_button_font']);
        } else {
            $value = $this->defaults['color_primary_button_font'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_primary_button_font]" value="%s" >', $value
        );
    }

    public function input_color_action_button_background_callback(){

        if(isset( $this->options['color_action_button_background'])){
            $value = esc_attr($this->options['color_action_button_background']);
        } else {
            $value = $this->defaults['color_action_button_background'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_action_button_background]" value="%s" >', $value
        );
    }

    public function input_color_action_button_background_shadow_callback(){

        if(isset( $this->options['color_action_button_background_shadow'])){
            $value = esc_attr($this->options['color_action_button_background_shadow']);
        } else {
            $value = $this->defaults['color_action_button_background_shadow'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_action_button_background_shadow]" value="%s" >', $value
        );
    }

    public function input_color_action_button_font_callback(){

        if(isset( $this->options['color_action_button_font'])){
            $value = esc_attr($this->options['color_action_button_font']);
        } else {
            $value = $this->defaults['color_action_button_font'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_action_button_font]" value="%s" >', $value
        );
    }

    public function input_color_forum_canvas_callback(){

        if(isset( $this->options['color_forum_canvas'])){
            $value = esc_attr($this->options['color_forum_canvas']);
        } else {
            $value = $this->defaults['color_forum_canvas'];
        }
        printf(
            '<input class="jscolor {hash:true}" name="'.MY_OPTION_NAME.'[color_forum_canvas]" value="%s" >', $value
        );
    }

    public function select_feature_home_callback(){
        $features = array();
        $features["-1"] = __('None', THEME_NAME);
        $features["0"] = __('Default', THEME_NAME);
        $wp_features = get_posts( 'numberposts=-1&post_type='.My_Features::POST_TYPE.'&post_status=publish&orderby=ID&order=ASC' );
        foreach($wp_features as $wp_feature) {
            $features[$wp_feature->ID] = $wp_feature->post_title;
        }
        $wp_features = null;
        $html = '<select name="'.MY_OPTION_NAME.'[feature_home]" >';
        foreach($features as $key => $feature){
            $html .= '<option value="'.$key.'" '.selected( $this->options['feature_home'],$key, false).'>'.$feature.'</option>';
        }
        $html .= '</select>';
        echo $html;
        
    }

    private function get_autofill_data($raw_value, $records){
        $saved_records = array();
        $ids = explode(",", $raw_value);
        if(count($records) > 0){
            foreach($ids as $id){
                foreach($records as $record){
                    if($id == $record->id){
                        $saved_records[] = $record;
                    }
                }
            }
        }
        return $saved_records;
    }

    public function input_categories_homepage_callback(){
        $wp_categories = get_categories(array(
                                        'orderby' => 'name',
                                        'order' => 'ASC',
                                        'child_of' => 0
                                        )); 
        $categories = array();
        foreach($wp_categories as $wp_category) { 
            
            $object = new StdClass();
            $object->id = $wp_category->term_id;
            $object->name = $wp_category->name;
            $categories[] = $object;
        }
        $wp_categories = null;

        $raw_value = $this->options['categories_homepage'];
        $saved_categories = $this->get_autofill_data($raw_value,$categories);

        ?>
        <input type="text" class="group-categories-homepage" id="categories-homepage" name="<?php echo MY_OPTION_NAME.'[categories_homepage]';?>" value="<?php echo $raw_value; ?>" ></input>
        <script>
        (function($){$("#categories-homepage").tokenInput(<?php echo json_encode($categories);?>,{prePopulate:<?php echo json_encode($saved_categories);?>});})(jQuery);
        </script>
        <?php
    }
}