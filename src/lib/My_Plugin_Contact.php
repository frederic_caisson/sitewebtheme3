<?php
class My_Plugin_Contact
{
	public function __construct(){
		
		add_shortcode( 'my_contact_form', array( $this , 'print_contact_form' ) );
	}

	public function print_contact_form(){
		
		$b = intval(date('d'));
		if ($b % 2){
			$b = $b + 1;
		} else {
			$b = $b + 2;
		}
		$a = intval(date('m'));
		if ($a % 2){
			$a = $a + 2;
		} else {
			$a = $a + 1;
		}
		$c = $a + $b;
		$crsf = md5(date('Ymd'));

		$name = '';
		$email = '';
		$subject = '';
		$message = '';
		$captcha = null;
		$sent = false;

		if(isset($_POST['contact_form_submit'])){
			$name = $_POST['contact_form_name'];
			$email = $_POST['contact_form_email'];
			$subject = $_POST['contact_form_subject'];
			$message = $_POST['contact_form_message'];

			$test = true;
			if(empty(trim($message))){
				
				$content = '<p style="background-color:red;padding:10px;color:white">';
				$content .= __('Your message is blank', THEME_NAME);
				$content .= '</p>';
				
				$test = false;
			}

			$captcha = $_POST['contact_form_captcha'];
			if($captcha != $c){
	
				$content .= '<p style="background-color:red;padding:10px;color:white">';
				$content .= __('The answer is wrong', THEME_NAME);
				$content .= '</p>';

				$test = false;
			}

			if($test){
				$to = get_bloginfo('admin_email');
				$header = "From:$email";
				$message_detail = __('Name', THEME_NAME).": $name \r\n";
				$message_detail = $message_detail . __('Email', THEME_NAME).": $email \r\n";
				$message_detail = $message_detail . __('Subject', THEME_NAME).": $subject \r\n\r\n";
				$message_detail = $message_detail . $message;
				wp_mail( $to, $subject, $message_detail, $header);
				$sent = true;

				$content .= '<p style="background-color:green;padding:10px;color:white">';
				$content .= __('Your message was sent', THEME_NAME);
				$content .= '</p>';
			}
		}
		if(!$sent){

			$content .= '<form method="post">';
			$content .= '<label>';
			$content .= __('Name', THEME_NAME);
			$content .= '</label><br>';

			$content .= '<input type="text" size="40" name="contact_form_name" value="';
			$content .= $name;
			$content .='" required>*<br>';
		
			$content .= '<label>';
			$content .= __('Email', THEME_NAME);
			$content .= '</label><br>';

			$content .= '<input type="email" size="40" name="contact_form_email" value="';
			$content .= $email;
			$content .='" required>*<br>';

			$content .= '<label>';
			$content .= __('Subject', THEME_NAME);
			$content .= '</label><br>';

			$content .= '<input type="text" size="40" name="contact_form_subject" value="';
			$content .= $subject;
			$content .='" required>*<br>';

			$content .= '<label>';
			$content .= __('Message', THEME_NAME);
			$content .= '</label><br>';

			$content .= '<textarea name="contact_form_message" cols="40" rows="5">';
			$content .= $message;
			$content .= '</textarea>*<br>';

			$content .= '<label>';
			$content .= __('Resolve the operation', THEME_NAME);
			$content .= '</label>';
			$content .= " $a + $b = ?";
			$content .= '<br>';
			$content .= '<input type="text" size="40" name="contact_form_captcha" required>*<br>';
			$content .= '<input type="submit" name="contact_form_submit">';
			$content .= '</form>';

		}
		return $content;
	}

}