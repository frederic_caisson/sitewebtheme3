<?php

class My_Posts {
	
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save' ) );
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_boxes( $post_type ) {
            $post_types = array('post', 'page');     //limit meta box to certain post types
            $comments_label = __( '% Comments', THEME_NAME );
            $comments_label = str_replace("% ", "", $comments_label);

            if ( in_array( $post_type, $post_types )) {
				add_meta_box(
					'post_metabox_hide_comments'
					,$comments_label
					,array( $this, 'render_meta_box_hide_comments' )
					, $post_type
					,'side'
					,'default'
				);
            }

            if($post_type == 'post'){
	            add_meta_box(
					'post_metabox_feature'
					,__( 'Feature', THEME_NAME )
					,array( $this, 'render_meta_box_feature' )
					,'post'
					,'normal'
					,'high'
				);
        	}
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
	
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['post_metabox_hide_comments_nonce'] ) )
			return $post_id;

		// Verify that the nonce is valid.
		
		if ( ! wp_verify_nonce( $_POST['post_metabox_hide_comments_nonce'], 'post_metabox_hide_comments' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {
			if ( ! isset( $_POST['post_metabox_feature_nonce'] ) )
				return $post_id;

			if ( ! wp_verify_nonce( $_POST['post_metabox_feature_nonce'], 'post_metabox_feature' ) )
				return $post_id;

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;

			$feature_columns = array('_feature_id' , 
								 	 '_feature_id2' );

			foreach($feature_columns as $feature_column){
				$feature_id = sanitize_text_field( $_POST[$feature_column] );
				update_post_meta( $post_id, $feature_column, $feature_id );
			}
		}

		if(empty($_POST['hide_comments'])){
			$hide_comments = 0;
		} else {
			$hide_comments = intval($_POST['hide_comments']);
		}
		update_post_meta( $post_id, '_hide_comments', $hide_comments );
	}

	public function render_meta_box_hide_comments( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'post_metabox_hide_comments', 'post_metabox_hide_comments_nonce' );

		// Use get_post_meta to retrieve an existing value from the database.
		$value = get_post_meta( $post->ID, '_hide_comments', true );
		$value = intval($value);

		// Display the form, using the current value.
		echo '<input type="checkbox" name="hide_comments" value="1" '.checked( $value, 1, false).'>&nbsp;';
	   	echo '<label for="hide_comment">'.__('Hide comments', THEME_NAME).'</label>';
	   
	}

	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_feature( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'post_metabox_feature', 'post_metabox_feature_nonce' );

		

		// Display the form, using the current value.
        $wp_features = get_posts( 'numberposts=-1&post_type='.My_Features::POST_TYPE.'&post_status=publish&orderby=ID&order=ASC' );


        $feature_columns = array('_feature_id' => 'top feature', 
								 '_feature_id2' => 'bottom feature');

        
        foreach($feature_columns as $feature_column => $feature_label){
        	// Use get_post_meta to retrieve an existing value from the database.
        	$value = get_post_meta( $post->ID, $feature_column, true );
		   	echo '<label for="'.$feature_column.'">'.__("Choose a $feature_label for this article", THEME_NAME).'</label><br>';
		   	echo '<select id="'.$feature_column.'" name="'.$feature_column.'">';
		   	echo '<option value="-1" '.selected($value, '-1' , false).'>'.__('None', THEME_NAME).'</option>';
			
			if(intval($value) == 0){
				$value = '0';
			}
		   	echo '<option value="0" '.selected($value, '0' , false).'>'.__('Default', THEME_NAME).'</option>';
		    
		   	foreach($wp_features as $wp_feature){
		   		$id = strval($wp_feature->ID);
		   		echo '<option value="'.$id.'" '.selected($value, $id , false).'>'.($wp_feature->post_title).'</option>';
		   	}
		   	echo '</select><br>';

        }
		


	}

}