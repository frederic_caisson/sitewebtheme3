<?php
include('lib/My_Menu.php');
include('lib/My_Admin.php');

class My_Theme {

	public static $instance;
	private $menu;
	private $admin;
	private $theme_name;

	const THUMB_WIDTH = 484;
	const THUMB_HEIGHT = 252;
	const POSTS_LIMIT = 5;
	const RELATED_LIMIT = 50;

    public static function get_instance() {
		if(is_null(self::$instance)) {
			self::$instance = new My_Theme();;
		}
		return self::$instance;
	}

	public function __destruct(){
    	if(!is_null(self::$instance)) {
    		self::$instance->clean_object();
        	self::$instance = null;
    	}
    }

    public function clean_object(){
    	$this->menu = null;
    	$this->admin = null;
    	$this->theme_name = null;
    }

	public function __construct(){
		$this->register_variables();
		$this->load_locale();
		$this->load_support();
		$this->register_sidebars();
		$this->load_hooks();
		$this->add_custom_size_image();
		$this->load_admin();
	}

	public function register_variables(){
		$theme_info = wp_get_theme();
		$this->theme_name = $theme_info->get('Name');
		$theme_info = null;

		define('THEME_NAME', $this->theme_name);
		define('MY_CSS_FILE_NAME', 'mystyle.css');
		define('MY_OPTION_NAME', 'my_theme_options');
	}

	public function load_admin(){
		if(is_null($this->admin)){
			$this->admin = new My_Admin();
		}
	}

	public function get_menu(){
		if(is_null($this->menu)){
			$this->menu = new My_Menu();
		}
		return $this->menu;
	}

	public function get_option($name){
		$options = get_option(MY_OPTION_NAME);
		if($options){
			if(array_key_exists($name,$options)){
				return $options[$name];
			}
		}
		return null;
	}

	public function get_comments($comment, $args, $depth){
		global $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
<div id="comment-<?php comment_ID(); ?>">
<div class="comment-author h-card vcard author" itemprop="name">
<div class="comment-avatar u-photo"><?php echo get_avatar($comment,$size='48'); ?></div>
<div class="comment-meta">
<span class="comment-author-name p-name fn"><?php printf(__('%s'), get_comment_author()) ?></span><span class="comment-time" itemprop="dateCreated">(<?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?>)</span>
</div>
</div>
<?php if ($comment->comment_approved == '0') : ?>
<div class="comment-triangle"></div>
<div class="p-note comment-text" itemprop="text">
<i><?php echo __('Your comment is awaiting moderation.') ?></i>
</div>
<?php else: ?>
<div class="comment-triangle"></div>
<div class="p-note comment-text" itemprop="text">
<?php comment_text() ?>
</div>
<div class="comment-buttons">
<?php edit_comment_link(__('Edit'),'  ','') ?> <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
</div>
<?php endif; ?>
</div>
		<?php
	}

	private function load_locale(){
		load_theme_textdomain(THEME_NAME,get_template_directory().'/languages');

		$locale = get_locale();
		$locale_file = get_template_directory()."/languages/$locale.php";
		if (is_readable($locale_file))
			require_once($locale_file);
	}

	private function load_hooks(){
		add_filter('excerpt_more', array($this,'customize_excerpt_more'));
		add_filter('dynamic_sidebar_params' , array($this,'customize_sidebar_params'));
		add_filter('post_gallery', array($this,'customize_gallery'));
		
    	add_filter('comment_form_default_fields', array($this,'customize_comment_form'));
    	add_filter('comment_form_field_comment', array($this,'customize_comment_textarea'));
    	add_filter('comment_form_defaults', array($this,'customize_comment_form_defaults'));
    	add_filter('get_search_form', array($this,'customize_search_form'));

    	add_filter('bbp_kses_allowed_tags', array($this,'customize_bbp_tags'));
    	add_action('wp', array($this,'baw_non_duplicate_content') );
		add_action('wp_enqueue_scripts', array($this,'prepare_scripts'));
		add_filter('style_loader_tag', array($this,'remove_id_scripts'));

		//Remove versions in javascript and css
		add_filter('style_loader_src', array($this,'remove_cssjs_ver'));
		add_filter('script_loader_src', array($this,'remove_cssjs_ver'));

		//Remove javascript and css in header
		remove_action('wp_head', 'wp_print_scripts', 9);
    	remove_action('wp_head', 'wp_print_head_scripts', 9);
    	remove_action('wp_head', 'wp_enqueue_scripts', 9);
    	remove_action('wp_head', 'wp_print_styles', 8);

    	//Add javascript and css in footer
    	add_action('wp_footer', 'wp_print_styles', 5);
    	add_action('wp_footer', 'wp_print_scripts', 5);
    	add_action('wp_footer', 'wp_enqueue_scripts', 5);
    	add_action('wp_footer', 'wp_print_head_scripts', 5);
	}

	public function customize_sidebar_params($params) {
		$h = $this->get_tag_for('tag_sidebar');
		$params[0]['before_title'] = "<$h class='widget-title'>";
		$params[0]['after_title'] = "</$h>";
		return $params;
	}

	public function customize_comment_form_defaults( $defaults ) {
		$h = $this->get_tag_for('tag_comment_title');
		$defaults["title_reply_before"] = "<$h id='reply-title' class='comment-reply-title'>";
		$defaults["title_reply_after"] = "</$h>";
	    return $defaults;
	}

	public function customize_gallery($output, $attr) {
	    global $post;

	    static $instance = 0;
	    $instance++;


	    if (isset($attr['orderby'])) {
	        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
	        if (!$attr['orderby'])
	            unset($attr['orderby']);
	    }

	    extract(shortcode_atts(array(
	        'order'      => 'ASC',
	        'orderby'    => 'menu_order ID',
	        'id'         => $post->ID,
	        'itemtag'    => 'dl',
	        'icontag'    => 'dt',
	        'captiontag' => 'dd',
	        'columns'    => 3,
	        'size'       => 'thumbnail',
	        'include'    => '',
	        'exclude'    => ''
	   ), $attr));

	    $id = intval($id);
	    if ('RAND' == $order)
	        $orderby = 'none';

	    if (!empty($include)) {
	        $include = preg_replace('/[^0-9,]+/', '', $include);
	        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

	        $attachments = array();
	        foreach ($_attachments as $key => $val) {
	            $attachments[$val->ID] = $_attachments[$key];
	        }
	    } elseif (!empty($exclude)) {
	        $exclude = preg_replace('/[^0-9,]+/', '', $exclude);
	        $attachments = get_children(array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
	    } else {
	        $attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
	    }

	    if (empty($attachments))
	        return '';

	    if (is_feed()) {
	        $output = "\n";
	        foreach ($attachments as $att_id => $attachment)
	            $output.= wp_get_attachment_link($att_id, $size, true)."\n";
	        return $output;
	    }

	    $itemtag = tag_escape($itemtag);
	    $captiontag = tag_escape($captiontag);
	    $columns = intval($columns);
	    $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	    $float = is_rtl() ? 'right' : 'left';

	    $selector = "gallery-{$instance}";

	    $gallery_style = $gallery_div = '';
	    if (apply_filters('use_default_gallery_style', true))
	        
	        $gallery_style = "
	        <style type='text/css'>
	            #{$selector} {
	                margin: auto;
	            }
	            #{$selector}.gallery-item {
	                float: {$float};
	                margin-top: 10px;
	                text-align: center;
	                width: {$itemwidth}%;
	            }
	            #{$selector} img {
	                border: 2px solid #cfcfcf;
	            }
	            #{$selector}.gallery-caption {
	                margin-left: 0;
	            }
	        </style>
	        <!-- see gallery_shortcode() in wp-includes/media.php -->";
	        
	    $size_class = sanitize_html_class($size);
	    $gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	    $output = apply_filters('gallery_style', $gallery_style."\n\t\t".$gallery_div);

	    $i = 0;
	    $attachment_ids = implode(",", array_keys($attachments));
	    $attachment_ids = urlencode(base64_encode($attachment_ids));

	    foreach ($attachments as $attachement_id => $attachment) {
	        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($attachement_id, $size, false, false) : wp_get_attachment_link($attachement_id, $size, true, false);
	        $html = new SimpleXMLElement($link);
			$href = $html['href'];
			$html = null;
			$new_href = $href."?gallery=$attachment_ids";
			$link = str_replace($href,$new_href,$link);

	        $output.= "<{$itemtag} class='gallery-item'>";
	        $output.= "
	            <{$icontag} class='gallery-icon'>
	                $link
	            </{$icontag}>";
	        /*
	         * This is the caption part so i'll comment that out
	         * #2 in question
	         */
	        /*
	        if ($captiontag && trim($attachment->post_excerpt)) {
	            $output.= "
	                <{$captiontag} class='wp-caption-text gallery-caption'>
	                ".wptexturize($attachment->post_excerpt)."
	                </{$captiontag}>";
	        }*/
	        $output.= "</{$itemtag}>";
	        if ($columns > 0 && ++$i % $columns == 0)
	            $output.= '<br style="clear: both" />';
	    }

	    /**
	     * this is the extra br you want to remove so we change it to jus closing div tag
	     * #3 in question
	     */
	    /*$output.= "
	            <br style='clear: both;' />
	        </div>\n";
	    */

	    $output.= "<div class='clear'></div></div>\n";
	    return $output;
	}

	public function prepare_scripts(){
		wp_enqueue_script("jquery");
		wp_enqueue_script('theme', get_template_directory_uri().'/js/public.js');
		wp_enqueue_style('theme', get_template_directory_uri().'/mystyle.css');
	}

	public function remove_id_scripts($link){
		return preg_replace("/id='.*-css'/", "", $link);
	}

	public function customize_excerpt_more($more){
		return ' &hellip;';
	}

	public function remove_cssjs_ver($src){
   		if(strpos($src,'?ver=')){
        	$src = remove_query_arg('ver', $src);
    	}
	    return $src;
	}

	public function customize_comment_form(){

		$req = get_option('require_name_email');

		$fields = array(
		'author' => '<p>'.'<label for="author">'.__('Name',THEME_NAME).'</label> '.($req ? '<span>*</span>' : '').
		'<input id="author" name="author" type="text" value="'.'" size="30"'.' placeholder ='.__('"What shall we call you?"', THEME_NAME).($req ? ' required' : '').'/></p>',

		'email'  => '<p><label for="email">'.__('Email',THEME_NAME).'</label> '.($req ? '<span>*</span>' : '').
		'<input id="email" name="email" type="email" value="'.'" size="30"'.' placeholder ='.__('"Leave us a valid email adress"', THEME_NAME).($req ? ' required' : '').' /></p>',

		'url'    => '<p><label for="url">'.__('Website',THEME_NAME).'</label>'.
		'<input id="url" name="url" type="url" value="'.'" size="30" placeholder='.__('"Have you got a nice website ?"', THEME_NAME).'/></p>'

		);
		return $fields;
	}

	public function customize_comment_textarea(){
		
		$commentArea = '<p><label for="comment">'.__('Comment', THEME_NAME).'</label><textarea id="comment" name="comment" cols="45" rows="8" placeholder='.__('"What is in your mind ?"', THEME_NAME).' required ></textarea></p>';
		return $commentArea;
	}

	public function customize_search_form($form) {
		$form = '<form class="searchform" role="search" method="get" action="'.home_url('/').'" itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
		<p><label class="screen-reader-text" >'.__('Search for:',THEME_NAME).'</label><br>
		<input type="search" value="'.get_search_query().'" name="s" autocomplete="on" placeholder ='.__('"What are you looking for?"', THEME_NAME). ' />
		<input type="submit" value="'. esc_attr__('Search').'" />
		</p>
		</form>';
		return $form;
	}

	public function print_search_input(){
		echo '<form class="searchform2" role="search" method="get" action="'.home_url('/').'" itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
		<input type="search" value="'.get_search_query().'" size="30" name="s" autocomplete="on" placeholder ='.__('"What are you looking for?"', THEME_NAME). ' />
		<i class="fa fa-search"></i></form>';	
	}

	private function load_support(){
		add_theme_support('automatic-feed-links');
		add_theme_support('post-thumbnails');
		add_theme_support('custom-header');
		add_theme_support('custom-background');
		add_editor_style("/css/editor-style.css");
	}

	public function pager() {
	    global $wp_query, $wp_rewrite;
	    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
	    $pagination = array(
	    'base' => @add_query_arg('page','%#%'),
	    'format' => '',
	    'total' => $wp_query->max_num_pages,
	    'current' => $current,
	    'show_all' => false,
	    'end_size' => 1,
	    'mid_size' => 2,
	    'type' => 'list',
	    'next_text' => __('Older posts', THEME_NAME).' &rarr;',
	    'prev_text' => '&larr; '.__('Newer posts', THEME_NAME)
	    );
	    if($wp_rewrite->using_permalinks())
	    $pagination['base'] = user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))).'page/%#%/', 'paged');
	    if(!empty($wp_query->query_vars['s']))
	    $pagination['add_args'] = array('s' => str_replace(' ' , '+', get_query_var('s')));
	    echo str_replace('page/1/','', paginate_links($pagination));
    }	

	private function register_sidebars(){
		register_nav_menus(array(
			'primary' => __('Primary Menu', THEME_NAME)
		));
		register_nav_menus(array(
			'footer' => __('Footer Menu', THEME_NAME)
		));

		register_sidebar(array (
			'name' => __('Left sidebar', THEME_NAME),
			'id' => 'left-sidebar',
			'before_widget' => '<div id="%1$s" class="sidebar-widget widget %2$s" role="complementary">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="widget-title">',
			'after_title' => '</h5>',
		));

		register_sidebar(array (
			'name' => __('Right sidebar', THEME_NAME),
			'id' => 'right-sidebar',
			'before_widget' => '<div id="%1$s" class="sidebar-widget widget %2$s" role="complementary">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="widget-title">',
			'after_title' => '</h5>',
		));

		register_sidebar(array(
			'name' => __('Footer', THEME_NAME),
			'id' => 'footer-widget-area',
			'description' => __('The footer area', THEME_NAME),
			'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="widget-title">',
			'after_title' => '</h5>',
		));
	}

	public function get_selected_categories(){
		$cat_ids = array();
		$setting_post_categories = $this->get_option('categories_homepage');
		if (!empty($setting_post_categories)) {
			$cat_ids = explode(',', $setting_post_categories);
		}
		return $cat_ids;
	}

	public function add_custom_size_image(){
    	add_image_size('thumb-fb', self::THUMB_WIDTH, self::THUMB_HEIGHT); 
	}

	public function remove_thumbnail_dimensions($html, $post_id, $post_image_id) {
    	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    	return $html;
	}

	public function get_microdata_schema() {
		$schema = 'http://schema.org/';
		if(is_single()) {
			$type = "Article";
		}
		elseif(is_home()||is_archive()||is_category()) {
			$type = 'Blog';
		}
		elseif(is_front_page()) {
			$type = 'Website';
		}
		else {
			$type = 'WebPage';
		}
		echo 'itemscope itemtype="'.$schema.$type.'"';
	}

	public function get_tag_for($name) {
		$tags = array();

		//Seo your titles
		$blog_description = get_bloginfo('description');

		//Case Home 
		if (is_home()) {
			$tags['tag_site_title'] = 'h1';
			$tags['tag_category_title'] = 'h2';
			$tags['tag_recent_post'] = 'h3';

			if (!empty($blog_description)) {
				$tags['tag_blog_description'] = 'h2';
				$tags['tag_recent_post'] = 'h3';
			}
			$tags['tag_sidebar'] = 'h4';
		
		//Case Single, Page	
		} else if (is_single() || is_page()) {

	        $content = get_post_field( 'post_content', get_the_ID() );
	        $smallerH = 2;
	        $found = false;
	        $mapH = array();
	        for($i=6;$i>=2;$i--){
	        	$pos = strripos($content, "</h$i>");
	        	if ($pos !== false) {
	        		 if($smallerH < $i){
	        		 	$found = true;
	        		 	$smallerH = $i;
	        		 }
	    		}
	        }
	        if(!empty($found)){
	        	if($smallerH < 6){
					$smallerH = $smallerH + 1;
				}
			} 
	       
			$tags['tag_recent_post'] = 'h1';
			$tags['tag_site_title'] = 'span';
			if(!empty($blog_description)){
				$tags['tag_blog_description'] = 'span';
			}
			$tags['tag_author'] = "h$smallerH";
			$tags['tag_related_post'] = "h$smallerH";
			$tags['tag_comment_title'] = "h$smallerH";
			$tags['tag_sidebar'] = "h$smallerH";

		//Category, Case Tag, Archive, Search
		} else if (is_category() || is_tag() || is_archive() || is_search() || is_404()) {

			$tags['tag_category_title'] = 'h1';
			$tags['tag_recent_post'] = 'h2';
			$tags['tag_site_title'] = 'h3';
			$tags['tag_blog_description'] = 'h4';
			$tags['tag_sidebar'] = "h5";

		} else {
			$tags['tag_site_title'] = 'h1';
			$tags['tag_category_title'] = 'h2';
			$tags['tag_recent_post'] = 'h3';

			if (!empty($blog_description)) {
				$tags['tag_blog_description'] = 'h2';
				$tags['tag_recent_post'] = 'h3';
			}
		}

		return $tags[$name];
	}

	public function breadcrumb(){
		global $post;
		$str ='';
		$sep = '&nbsp;&raquo;&nbsp;';
		$home = __('Home', THEME_NAME);
		$check_bbpress = false;
		if(function_exists('is_bbpress')){
		    $check_bbpress = is_bbpress();
		}
		if(!is_home()&&!is_admin()&&!$check_bbpress){
			$count = 1;
		    $str.= '<div class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList"><div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		    $str.= '<a href="'. home_url().'" itemprop="item"><span itemprop="name">'.$home.'</span></a><meta itemprop="position" content="'.$count.'" />'.$sep.'</div>';
		    
		    if(is_tag()){
		    	$tag_id = get_query_var('tag_id');
		    	$count++;
		    	$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		        $str.='<a href="'. get_tag_link($tag_id). '" itemprop="item"><span itemprop="name">'. single_tag_title('',false). '</span></a><meta itemprop="position" content="'.$count.'" /></div>';
			} elseif(is_category()) {
		        $cat = get_queried_object();
		        if($cat -> parent != 0){
		            $ancestors = array_reverse(get_ancestors($cat -> cat_ID, 'category'));
		            $max = count($ancestors);
		            foreach($ancestors as $ancestor){
		            	$count++;

		                $str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		                $str.='<a href="'. get_category_link($ancestor).'" itemprop="item"><span itemprop="name">'. get_cat_name($ancestor).'</span></a><meta itemprop="position" content="'.$count.'" />'.$sep.'</div>';	
		            }
		            $itemref = '';
		        }
		        $count++;
		    	$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		    	$str.= '<a href="'. get_category_link($cat -> term_id). '" itemprop="item"><span itemprop="name">'. $cat-> cat_name.'</span></a><meta itemprop="position" content="'.$count.'" /></div>';

		    } elseif(is_page()){
		        if($post -> post_parent != 0){
		            $ancestors = array_reverse(get_post_ancestors($post->ID));
		            
		            $max = count($ancestors);
		            foreach($ancestors as $ancestor){
		            	$count++;
		            	$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		                $str.='<a href="'. get_permalink($ancestor).'" itemprop="item"><span itemprop="name">'. get_the_title($ancestor).'</span></a><meta itemprop="position" content="'.$count.'" />'.$sep.'</div>';
		            }
		        }
		        $count++;
		        $str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		    	$str.= '<a href="'. get_permalink(). '" itemprop="item"><span itemprop="name">'. get_the_title().'</span></a><meta itemprop="position" content="'.$count.'" /></div>';
		    } elseif(is_single()){
		        $categories = get_the_category($post->ID);
		        $cat = $categories[0];
		        if($cat -> parent != 0){
		            $ancestors = array_reverse(get_ancestors($cat -> cat_ID, 'category'));
		            $i = 0;
		            $max = count($ancestors);
		            foreach($ancestors as $ancestor){
		            	$count++;
		                $str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		                $str.='<a href="'. get_category_link($ancestor).'" itemprop="item"><span itemprop="name">'. get_cat_name($ancestor). '</span></a><meta itemprop="position" content="'.$count.'" />'.$sep.'</div>';
		            }
		        }
		        $count++;
		        $str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		        $str.='<a href="'. get_category_link($cat -> term_id). '" itemprop="item"><span itemprop="name">'. $cat-> cat_name.'</span></a><meta itemprop="position" content="'.$count.'" /></div>';
		    } elseif(is_archive()){
		    	if (is_day()){
					$cat_title = get_the_date('Y/m/d');
				} elseif (is_month()) {
					$cat_title = get_the_date('Y/m');
				} elseif (is_year()) {
					$cat_title = get_the_date('Y');
				} else {
					$cat_title =  __('Blog Archives', THEME_NAME);
				}
				$count++;
				$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
		        $str.='<a href="'. site_url('/'.$cat_title). '" itemprop="item"><span itemprop="name">'. $cat_title.'</span></a><meta itemprop="position" content="'.$count.'" /></div>';

		    } else {
		        //$str.='<div>'. wp_title('', false).'</div>';
		    }
		    $str.='</div>';

		}
		echo $str;
	}

	public function hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);

	   if(strlen($hex) == 3) {
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   
	   return $rgb;
	}

	public function current_url() {
		$url = @($_SERVER["HTTPS"] != 'on') ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
		$url.= ($_SERVER["SERVER_PORT"] !== 80) ? ":".$_SERVER["SERVER_PORT"] : "";
		$url.= $_SERVER["REQUEST_URI"];
		return $url;
	}

	public function get_feature_background($param){
		if($param == 'top'){
			$feature_column = '_feature_id';
			$key = My_Features::FIELD_DEFAULT_FEATURE;
		} elseif ($param == 'bottom') {
			$feature_column = '_feature_id2';
			$key = My_Features::FIELD_DEFAULT_FEATURE2;
		}

		$thumb = '';
		if(is_home()){
			if($param == 'top'){
            	$feature_id = $this->get_option('feature_home');
        	} else {
        		return $thumb;
        	}
        } elseif(is_single()){
            $post_id = get_the_ID();
            $feature_id = get_post_meta($post_id , $feature_column, true);
        }
        if(intval($feature_id) != 0){
            if(intval($feature_id)>0){
            	$thumb_id = get_post_thumbnail_id($feature_id);
            	if($thumb_id){
            		$array = wp_get_attachment_image_src($thumb_id, 'full');
            		$thumb = $array[0];
            	}
            }
        }  else {
            $args = array(
                'posts_per_page' => 1,
                'post_type' => My_Features::POST_TYPE,
                'meta_query' => array(
                    array(
                        'key' => $key,
                        'value' => "1",
                   )
               )
           );
            $wp_features = get_posts($args);
            if(count($wp_features) > 0){
            	$feature_id = $wp_features[0]->ID;
				$thumb_id = get_post_thumbnail_id($feature_id);
            	if($thumb_id){
            		$array = wp_get_attachment_image_src($thumb_id, 'full');
            		$thumb = $array[0];
            	}
            }
        }
        return $thumb;
	}

	public function is_hide_comments(){

		if(is_page()){
			$option = 'hide_comments_on_page';
		} else {
			$option = 'hide_comments_on_post';
		}

		$general_hide_comments = $this->get_option($option);
		if(!empty($general_hide_comments)){
		    return true;
		}
		
		$post_id = get_the_ID();
        $hide_comments = get_post_meta($post_id , '_hide_comments', true);
        if(empty($hide_comments)){
        	return false;
        } else {
        	return true;
        }
	}

	public function is_hide_author(){
		if(is_page()){
			$option = 'hide_author_on_page';
		} else {
			$option = 'hide_author_on_post';
		}

		$general_hide_author = $this->get_option($option);
		if(!empty($general_hide_author)){
		    return true;
		}
		return false;
	}

	public function is_show_related_posts(){
		
		$option = 'show_related_posts_on_post';
		
		$general_show_related_posts = $this->get_option($option);
		if(!empty($general_show_related_posts)){
		    return true;
		}
		return false;
	}

	public function get_loop_template(){

		$articles_organization = $this->get_option('articles_organization');
		$articles_organization = intval($articles_organization);

		switch($articles_organization){
			case 1:
				$part = 'articles';
			break;
			case 2:
				$part = 'featured';
			break;
			case 3:
				$part = 'blocks';
			break;
			default: 
				$part = 'articles';
			break;
		}

		if(!is_home()){
			if($part == 'featured'){
				$part = 'articles';
			}
		}

		get_template_part('loop', $part);
	}

	public function get_feature($param){
		$feature_content = '';

		if($param == 'top'){
			$feature_column = '_feature_id';
			$key = My_Features::FIELD_DEFAULT_FEATURE;
		} elseif ($param == 'bottom') {
			$feature_column = '_feature_id2';
			$key = My_Features::FIELD_DEFAULT_FEATURE2;
		}
		if(isset($feature_column)){
			
	        if(is_home()){
	        	if($param == 'top'){
	            	$feature_id = $this->get_option('feature_home');
	            } else { 
	            	return $feature_content;
	            }
	        } elseif(is_single()){
	            $post_id = get_the_ID();
	            $feature_id = get_post_meta($post_id , $feature_column, true);
	        }
	        if(intval($feature_id) != 0){
	            if(intval($feature_id)>0){
	                $wp_feature = get_post($feature_id); 
	                $feature_content = $wp_feature->post_content;
	            }
	        } else {
	            $args = array(
	                'posts_per_page' => 1,
	                'post_type' => My_Features::POST_TYPE,
	                'meta_query' => array(
	                    array(
	                        'key' => $key,
	                        'value' => "1",
	                   )
	               )
	           );
	            $wp_features = get_posts($args);
	            if(count($wp_features) > 0){
	            	$feature_content = $wp_features[0]->post_content;
	            }
	        }
    	}
        return $feature_content;
	}

	public function get_attachement_title(){
		$title = get_the_title();
		$replace_chars = array("-", "_");
		$title = str_replace($replace_chars, " ", $title);
		$title = ucfirst($title);
		return $title;
	}

	public function customize_bbp_tags() {
		return array(
		 
		// Quotes
		'blockquote' => array(
		'cite' => array()
		),
		 
		// Code
		'code' => array(),
		 
		// Formatting
		'em' => array(),
		'strong' => array(),
		
		// Lists
		'ul' => array(),
		'li' => array(),
		 
		// Images
		'img' => array(
		'src' => true,
		'border' => true,
		'alt' => true,
		'height' => true,
		'width' => true,
		)
		);
	}

	public function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if (mb_strlen($excerpt) > $charlength) {
			$subex = mb_substr($excerpt, 0, $charlength - 5);
			$exwords = explode(' ', $subex);
			$excut = - (mb_strlen($exwords[ count($exwords) - 1 ]));
			if ($excut < 0) {
				echo mb_substr($subex, 0, $excut);
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $excerpt;
		}
	}

	public function baw_non_duplicate_content( $wp ){
		global $wp_query;
		// Si le nom de catégorie trouvée est différente entre le rewrite match et la variable requetée, alors on redirige
		if( isset( $wp_query->query_vars['category_name'], $wp_query->query['category_name'] )
		&& $wp_query->query_vars['category_name'] != $wp_query->query['category_name'] ):
		// L'URL correcte dans laquelle on remplace la catégorie requetée par son véritable nom
		$correct_url = str_replace( $wp_query->query['category_name'], $wp_query->query_vars['category_name'], $wp->request );
		// Redirection 301
		wp_redirect( home_url( $correct_url ), 301 );
		// Toujours die() après une redirection
		die();
		endif;
	}

	public function related_posts(){
		$related_post_ids = array();
		$h = $this->get_tag_for('tag_related_post'); 
    	$first = true;
        global $post;
        $orig_post = $post;
        $post_id = $post->ID;

        $tags = wp_get_post_tags($post_id);

        if ($tags) {
            $tag_ids = array();
        	foreach($tags as $tag){
        		$tag_ids[] = $tag->term_id;
        	}

        	$args=array(
                'tag__in' => $tag_ids,
                'post__not_in' => array($post_id),
                'posts_per_page'=> self::RELATED_LIMIT,
                'fields' => 'ids'
            );
        	$my_query = new wp_query($args);
       		foreach($my_query->posts as $id){
       			if(!array_key_exists($id,$related_post_ids)){
       				$related_post_ids[$id] = 0;
       			}

       			$tags2 = wp_get_post_tags($id);
       			foreach($tags2 as $tag){
        			if(in_array($tag->term_id,$tag_ids)){
        				$related_post_ids[$id] = $related_post_ids[$id] + 1;
        			}
        		}
       		}
        }

        $category_ids = wp_get_post_categories($post_id);

        if ($category_ids) {
            
        	$args=array(
                'category__in' => $category_ids,
                'post__not_in' => array($post_id),
                'posts_per_page'=> self::RELATED_LIMIT,
                'fields' => 'ids'
            );
        	$my_query = new wp_query($args);
       		foreach($my_query->posts as $id){
       			if(!array_key_exists($id,$related_post_ids)){
       				$related_post_ids[$id] = 0;
       			}
       			
       			$category_ids2 = wp_get_post_categories($id);
       			foreach($category_ids2 as $category_id){
        			if(in_array($category_id, $category_ids)){
        				$related_post_ids[$id] = $related_post_ids[$id] + 1;
        			}
        		}
       		}
        }

        if(count($related_post_ids)>0){
        	arsort($related_post_ids);
        	$first = true;
        	$count = 0;
        	foreach($related_post_ids as $related_post_id => $related_indice){
        		$count++;

        		if($count > 4){
        			break;
        		}

        		$related_post = get_post($related_post_id);
        		$permalink = get_permalink($related_post_id);
        		if($first){
       				echo '<'.$h.' class="related-title"><i class="fa fa-hand-o-right"></i> '.__("Related posts",THEME_NAME).'</'.$h.'>';
       				echo '<ul>';
       				$first = false;
       			}

       			echo '<li>';
        		if(has_post_thumbnail($related_post_id)){
        			echo '<div><a href="'.$permalink.'">';
        			$thumb = get_the_post_thumbnail($related_post_id,'thumbnail');
        			echo $thumb;
        			echo '</a><div>';
        		} else {
        			echo '<div><a href="'.$permalink.'">';
        			echo '<img src="'.get_template_directory_uri().'/images/no-thumb-150.jpg'.'">';
        			echo '</a><div>';
        		}
            	echo '<div><a href="'.$permalink.'" itemprop="sameAs">';
            	echo $related_post->post_title;
            	echo '</a></div>';
           		echo '</li>';
           		$related_post = null;

        	}
        	if(!$first){
    			echo "</ul>";
    		}
        }

        $post = $orig_post;
        wp_reset_query();
	}
}